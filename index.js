const express = require("express");
const http = require("http");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const session = require("express-session");
const flash = require("connect-flash");
const WebSocket = require("ws");
const cookieParser = require("cookie-parser");
const authRouter = require("./Router/auth");
const dashboardRouter = require("./Router/dashboard");
const videosRouter = require("./Router/videos");
const GoogleDriveRouter = require("./Router/videos/googledrive");
const RemoteM3u8Router = require("./Router/videos/remotem3u8");
const RemoteMP4Router = require("./Router/videos/remotemp4");
const GoogleRouter = require("./Router/googledrive");
const ApiRouter = require("./Router/api");
const LogsApiRouter = require("./Router/api/logs");
const ServerRouter = require("./Router/server");
const playRouter1 = require("./Router/play");
const playRouter2 = require("./Router/play/r2");
const playRouteftp = require("./Router/play/ftp");
const SettingRouter = require("./Router/setting");
const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

const corsOptions = {
  origin: [process.env.FRONTEND_URL], //included origin as true
  credentials: true, //included credentials as true
};
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "10mb" }));

app.use(
  session({
    secret: "your_secret_key",
    resave: false,
    saveUninitialized: false, // Change to false to prevent uninitialized sessions
    cookie: { maxAge: 10800000 }, // Session expiration time set to 3 hours
  })
);

app.use(flash());
app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "dist")));
app.use("/files", express.static(path.join(__dirname, "files")));

// Redirect the root URL to the login page
app.get("/", (req, res) => {
  res.redirect("/auth/login");
});

// Use the routers
app.use("/auth", authRouter);
app.use("/", dashboardRouter);
app.use("/", ApiRouter);
app.use("/", LogsApiRouter(wss)); // Pass wss to the API router creator function
app.use(
  "/",
  videosRouter,
  GoogleDriveRouter,
  RemoteM3u8Router,
  RemoteMP4Router
);
app.use("/", ServerRouter);
app.use("/", playRouter1, playRouter2, playRouteftp);
app.use("/", SettingRouter);
app.use("/", GoogleRouter);

// WebSocket connection setup
let storedLogData = []; // Array for storing log data

wss.on("connection", (ws) => {
  // Send all existing logs to the newly connected client
  ws.send(JSON.stringify(storedLogData));

  // Handle incoming messages if needed
  ws.on("message", (message) => {
    // Handle the message
  });

  // Handle errors
  ws.on("error", (error) => {
    console.error("WebSocket error:", error);
  });

  // Handle close event
  ws.on("close", () => {
    console.log("WebSocket connection closed");
  });
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
