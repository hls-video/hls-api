// models/domain.js
module.exports = (sequelize, DataTypes) => {
    const Domain = sequelize.define('Domain', {
      domainName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      defaultBgImage: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      hideLogo: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      logoURL: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      logoLink: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      logoPosition: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      adActive: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      iMemberID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'users', // Name of the table containing the user records
          key: 'id'
        }
      },
    });
  
    Domain.associate = models => {
      Domain.belongsTo(models.User, {
        foreignKey: 'iMemberID',
        as: 'user',
      });
      Domain.hasMany(models.AdField, {
        foreignKey: 'domainId',
        as: 'adFields',
      });
    };
  
    return Domain;
  };
  