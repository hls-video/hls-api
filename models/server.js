const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const Server = sequelize.define('Server', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(225),
      allowNull: false
    },
    ip: {
      type: DataTypes.STRING(225),
      allowNull: false
    },
    domains: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    domainsup: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    upload: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    TotalStorage: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '0'
    },
    UseStorage: {
      type: DataTypes.STRING(225),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'server',
    timestamps: false
  });

  return Server;
};
