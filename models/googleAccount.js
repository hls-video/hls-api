const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const GoogleAccount = sequelize.define('GoogleAccount', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    clientId: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    clientsecret: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    refreshtoken: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    created_at: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: DataTypes.NOW.toString()
    }
  }, {
    tableName: 'google_accounts',
    timestamps: false
  });

  return GoogleAccount;
};
