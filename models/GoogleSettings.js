// models/GoogleSettings.js
const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const GoogleSettings = sequelize.define('GoogleSettings', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    ip: {
      type: DataTypes.STRING,
      allowNull: true
    },
    port: {
      type: DataTypes.STRING,
      allowNull: true
    },
    username: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    reprocess360: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    reprocess720: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    reprocess1080: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    origin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    proxy360: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    proxy720: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    proxy1080: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    proxyhigh1080: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    proxyhigh720: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
    proxyhigh1080_360: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
    noproxy: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    tableName: 'google_settings',
    timestamps: false
  });

  GoogleSettings.associate = function(models) {
    GoogleSettings.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user'
    });
  };

  return GoogleSettings;
};
