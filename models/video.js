const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const Video = sequelize.define('Video', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    TotalSize: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: '0'
    },
    strDisk: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    iStatus: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    iUpload: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    iErrorCount: {
      type: DataTypes.TINYINT,
      allowNull: true,
      defaultValue: 0
    },
    iSliceCount: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    strUrl: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    strReferer: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    strSubtitles: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    iType: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    strServer: {
      type: DataTypes.INTEGER, // Changed to INTEGER to match the referenced column type
      allowNull: false,
      references: {
        model: 'server', // name of Target model
        key: 'id',       // key in Target model that we're referencing
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    },
    iMemberID: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    strTitle: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    strFileName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    strQualities: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    timeadd: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW
    },
    timeupdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW,
      onUpdate: DataTypes.NOW
    },
    md5: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
  
  }, {
    tableName: 'videos',
    timestamps: false
  });

  return Video;
};
