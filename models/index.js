require('dotenv').config();
const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  dialect: 'mysql',
  logging: false, // Disable logging
});

// Import models
const User = require('./user')(sequelize, DataTypes);
const Server = require('./server')(sequelize, DataTypes);
const Video = require('./video')(sequelize, DataTypes);
const GoogleAccount = require('./googleAccount')(sequelize, DataTypes);
const GoogleSettings = require('./GoogleSettings')(sequelize, DataTypes);
const PlayerSettings = require('./playerSettings')(sequelize, DataTypes);
const Domain = require('./domain')(sequelize, DataTypes);
const AdField = require('./adField')(sequelize, DataTypes); // Import AdField model

// Define relationships
Server.hasMany(Video, { foreignKey: 'strServer' });
Video.belongsTo(Server, { foreignKey: 'strServer' });

Domain.belongsTo(User, { foreignKey: 'iMemberID', as: 'user' });
Domain.hasMany(AdField, { foreignKey: 'domainId', as: 'adFields' });
AdField.belongsTo(Domain, { foreignKey: 'domainId', as: 'domain' });

sequelize.sync();

module.exports = { sequelize, User, Server, Video, GoogleAccount, GoogleSettings, PlayerSettings, Domain, AdField };
