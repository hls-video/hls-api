const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const PlayerSettings = sequelize.define('PlayerSettings', {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    playerUrl: {
      type: DataTypes.STRING,
      allowNull: false
    },
    iframeHeight: {
      type: DataTypes.STRING,
      allowNull: false
    },
    iframeWidth: {
      type: DataTypes.STRING,
      allowNull: false
    },
    jwplayerKey: {
      type: DataTypes.STRING,
      allowNull: false
    },
    jwplayerUrl: {
      type: DataTypes.STRING,
      allowNull: false
    },
    defaultImage: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rememberPosition: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    logoUrl: {
      type: DataTypes.STRING,
      allowNull: false
    },
    logoTarget: {
      type: DataTypes.STRING,
      allowNull: false
    },
    logoPosition: {
      type: DataTypes.STRING,
      allowNull: false
    },
    allowedSites: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    streamLinkDomains: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    p2pLink: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    streamApiKey: {
      type: DataTypes.STRING,
      allowNull: false
    },
    playerRememberPosition: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    logoSwitchChack: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    logoSwitchHide: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    allowedPlaylist: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    allowedPlaylistIndex: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    allowedP2P: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    allowedApiNuxt: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'player_settings'
  });

  return PlayerSettings;
};
