// models/adField.js
module.exports = (sequelize, DataTypes) => {
    const AdField = sequelize.define('AdField', {
      adURL: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      adLink: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      adTime: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      domainId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'domains', // Name of the table containing the domain records
          key: 'id'
        }
      },
    });
  
    AdField.associate = models => {
      AdField.belongsTo(models.Domain, {
        foreignKey: 'domainId',
        as: 'domain',
      });
    };
  
    return AdField;
  };
  