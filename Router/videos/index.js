// videos/index.js

require("dotenv").config();
const express = require("express");
const router = express.Router();
const { Op } = require("sequelize");
const axios = require("axios");
const moment = require("moment");
const { Server, Video } = require("../../models"); // Import Sequelize models

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  let userIdCookie = req.cookies.id;
  if (tokenCookie) {
    req.session.user = {
      id: userIdCookie,
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

router.get("/videos", isAuthenticated, async (req, res) => {
  try {
    const BaseUrl = process.env.BaseUrl;
    // res.render("videos/index", {
    //   BaseUrl,
    //   username: req.session.user.username,
    //   apiKey: req.session.user.api_key,
    // });
    return res.status(200).json({
      BaseUrl,
      username: req.session.user.username,
      apiKey: req.session.user.api_key,
    });
  } catch (err) {
    console.error("Error fetching video data:", err);
    res.status(500).send("Error fetching video data");
  }
});

router.get("/upload", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    res.render("videos/upload", {
      servers,
      username: req.session.user.username,
      userid: req.session.user.id,
      apiKey: req.session.user.api_key,
    });
  } catch (err) {
    res.status(500).send("Error fetching video data");
  }
});

router.post("/reset-video", isAuthenticated, async (req, res) => {
  const { id, TotalSize, iStatus, timeUpdate } = req.body;

  // Ensure TotalSize is not null
  const totalSize = TotalSize || 0; // Default to 0 if TotalSize is null or undefined

  const datetime = moment(timeUpdate).format("YYYY-MM-DD HH:mm:ss");

  try {
    // Fetch Video by id
    const video = await Video.findOne({ where: { id: id } });
    if (!video) {
      return res.status(404).json({ message: "Video not found" });
    }

    // Fetch Server by strServer from video
    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).json({ message: "Server not found" });
    }

    const [updated] = await Video.update(
      { TotalSize: totalSize, iStatus, timeupdate: datetime },
      { where: { id, iMemberID: req.session.user.id } }
    );

    if (updated) {
      res.json({ success: true, message: "Data updated successfully" });
    } else {
      console.error("Error:", err);
      res.json({ success: false, message: "Error updating data" });
    }
  } catch (err) {
    console.error("Error:", err);
    res.json({ success: false, message: "Error updating data" });
  }
});

router.delete("/delete-video/:id", isAuthenticated, async (req, res) => {
  const videoId = req.params.id;

  try {
    // Fetch Video by id
    const video = await Video.findOne({ where: { id: videoId } });
    if (!video) {
      return res.status(404).json({ message: "Video not found" });
    }

    // Fetch Server by strServer from video
    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).json({ message: "Server not found" });
    }

    // Send request to delete video via API
    await axios.delete(`http://${server.ip}/DeleteVideo`, {
      data: {
        id: video.md5,
        key: req.session.user.api_key,
      },
    });

    const deleted = await Video.destroy({
      where: { id: videoId, iMemberID: req.session.user.id },
    });

    if (deleted) {
      res.json({ success: true, message: "Video deleted successfully" });
    } else {
      res.status(404).json({ success: false, message: "Video not found" });
    }
  } catch (err) {
    console.error("Error deleting video:", err);
    res.status(500).json({ success: false, message: "Error deleting video" });
  }
});

router.post("/edit-video", isAuthenticated, async (req, res) => {
  const { id, title, url, iStatus } = req.body;

  try {
    const [updated] = await Video.update(
      { strTitle: title, strUrl: url, iStatus },
      { where: { id, iMemberID: req.session.user.id } }
    );

    if (updated) {
      res.json({ success: true, message: "Video updated successfully" });
    } else {
      res.status(404).json({ success: false, message: "Video not found" });
    }
  } catch (err) {
    console.error("Error updating video:", err);
    res.status(500).json({ success: false, message: "Error updating video" });
  }
});

router.post("/search-videos", isAuthenticated, async (req, res) => {
  let {
    page = 1,
    limit = 100,
    s_hash,
    s_strTitle,
    s_strUrl,
    s_iType,
    s_iStatus,
    s_strServer,
  } = req.body;
  const iMemberID = req.session.user.id;

  // Convert empty strings to undefined
  s_hash = s_hash || undefined;
  s_strTitle = s_strTitle || undefined;
  s_strUrl = s_strUrl || undefined;
  s_iType = s_iType || undefined;
  s_iStatus = s_iStatus || undefined;
  s_strServer = s_strServer || undefined;

  const where = {
    iMemberID,
    ...(s_hash && { md5: s_hash }),
    ...(s_strTitle && { strTitle: { [Op.like]: `%${s_strTitle}%` } }),
    ...(s_strUrl && { strUrl: { [Op.like]: `%${s_strUrl}%` } }),
    ...(s_iType && { iType: s_iType }),
    ...(s_iStatus && { iStatus: s_iStatus }),
    ...(s_strServer && { strServer: s_strServer }),
  };

  try {
    const { count: total, rows: videos } = await Video.findAndCountAll({
      where,
      limit: parseInt(limit, 10), // Ensure limit is an integer
      offset: (page - 1) * parseInt(limit, 10), // Ensure offset is an integer
      order: [["id", "DESC"]],
      include: [{ model: Server, attributes: ["name"] }],
    });

    const totalPages = Math.ceil(total / limit);
    const result = { videos, totalPages };

    res.json(result);
  } catch (error) {
    console.error("Error fetching videos:", error);
    res.status(500).json({ error: "An error occurred while fetching data" });
  }
});

router.get("/server-groups", isAuthenticated, async (req, res) => {
  try {
    const serverGroups = await Server.findAll({
      attributes: ["id", "name"],
    });

    res.json(serverGroups);
  } catch (error) {
    console.error("Error:", error);
    res
      .status(500)
      .json({ error: "An error occurred while fetching server groups" });
  }
});

module.exports = router;
