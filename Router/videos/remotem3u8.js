require("dotenv").config();
const express = require("express");
const router = express.Router();
const crypto = require("crypto");
const { Server, Video, User } = require("../../models"); // Import models from Sequelize
const Redis = require('ioredis');

const redisClient = new Redis({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  db: process.env.REDIS_DB,
  password: process.env.REDIS_PASSWORD || '', // ใช้ password ถ้ามี
  username: process.env.REDIS_USERNAME || '', // ใช้ username ถ้ามี
});

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  let userIdCookie = req.cookies.id;
  if (tokenCookie) {
    req.session.user = {
      id: userIdCookie,
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// มิดเดิลแวร์สำหรับแคช
const cacheMiddleware = (keyPrefix) => {
  return async (req, res, next) => {
    const key = `${keyPrefix}:${JSON.stringify(req.query || req.body)}`;
    try {
      const data = await redisClient.get(key);
      if (data) {
        res.json(JSON.parse(data));
      } else {
        res.sendResponse = res.json;
        res.json = (body) => {
          redisClient.setex(key, 3600, JSON.stringify(body)); // แคชข้อมูลเป็นเวลา 1 ชั่วโมง
          res.sendResponse(body);
        };
        next();
      }
    } catch (error) {
      console.error("Redis error:", error);
      next();
    }
  };
};

// ลบแคชเมื่อมีการอัปเดต หรือเพิ่มข้อมูลใหม่
const clearCache = async (prefix) => {
  try {
    const keys = await redisClient.keys(`${prefix}:*`);
    if (keys.length > 0) {
      await redisClient.del(keys);
    }
  } catch (err) {
    console.error("Error clearing cache:", err);
  }
};

// Route for displaying the remote m3u8 upload page
router.get(
  "/remote-m3u8",
  isAuthenticated,
  cacheMiddleware("remote-m3u8"),
  async (req, res) => {
    try {
      const servers = await Server.findAll();
      // res.render("videos/remote-m3u8", {
      //   servers,
      //   username: req.session.user.username,
      //   userid: req.session.user.id,
      //   apiKey: req.session.user.apiKey,
      // });
      return res.status(200).json({
        servers,
        username: req.session.user.username,
        userid: req.session.user.id,
        apiKey: req.session.user.apiKey,
      });
    } catch (err) {
      res.status(500).send("Error fetching server data");
    }
  }
);

// Route for adding a remote m3u8 video
router.post("/api/add-remote-m3u8", isAuthenticated, async (req, res) => {
  try {
    const { title, driveLink, serverId, iMemberID } = req.body;

    // Generate MD5 hash of the file ID
    const hashmd5id = crypto.createHash("md5").update(driveLink).digest("hex");
    const drivefull = driveLink;
    const strServer = serverId;
    const timeadd = new Date();
    const timeupdate = new Date();
    const md5 = hashmd5id;
    const iType = "remote-m3u8"; // Define the type
    const strTitle = title;
    const strUrl = drivefull;

    // Check if the link already exists
    const existingVideo = await Video.findOne({ where: { md5 } });

    if (existingVideo) {
      return res
        .status(400)
        .send("This remote-m3u8 link already exists in the database");
    }

    // Insert the new video data
    await Video.create({
      strServer,
      iMemberID,
      strTitle,
      timeadd,
      timeupdate,
      md5,
      iType,
      strUrl,
    });

    // ลบแคชที่เกี่ยวข้อง
    await clearCache("search-videos");
    await clearCache("remote-m3u8");
    res.status(200).send("Video data inserted successfully");
  } catch (err) {
    res.status(500).send("Error inserting video data");
  }
});

module.exports = router;
