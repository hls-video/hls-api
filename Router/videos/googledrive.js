require("dotenv").config();
const express = require("express");
const router = express.Router();
const { Server, Video, User } = require("../../models"); // นำเข้าโมเดลจาก Sequelize
const crypto = require("crypto");
const { check, validationResult, query } = require("express-validator");
const Redis = require('ioredis');

const redisClient = new Redis({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  db: process.env.REDIS_DB,
  password: process.env.REDIS_PASSWORD || '', // ใช้ password ถ้ามี
  username: process.env.REDIS_USERNAME || '', // ใช้ username ถ้ามี
});

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  if (tokenCookie) {
    req.session.user = {
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// มิดเดิลแวร์สำหรับแคช
const cacheMiddleware = (keyPrefix) => {
  return async (req, res, next) => {
    const key = `${keyPrefix}:${JSON.stringify(req.query || req.body)}`;
    try {
      const data = await redisClient.get(key);
      if (data) {
        res.json(JSON.parse(data));
      } else {
        res.sendResponse = res.json;
        res.json = (body) => {
          redisClient.setex(key, 3600, JSON.stringify(body)); // แคชข้อมูลเป็นเวลา 1 ชั่วโมง
          res.sendResponse(body);
        };
        next();
      }
    } catch (error) {
      console.error("Redis error:", error);
      next();
    }
  };
};

// ลบแคชเมื่อมีการอัปเดต หรือเพิ่มข้อมูลใหม่
const clearCache = async (prefix) => {
  try {
    const keys = await redisClient.keys(`${prefix}:*`);
    if (keys.length > 0) {
      await redisClient.del(keys);
    }
  } catch (err) {
    console.error("Error clearing cache:", err);
  }
};

// Route for displaying the Google Drive upload page
router.get(
  "/remote-google-drive",
  isAuthenticated,
  cacheMiddleware("remote-google-drive"),
  async (req, res) => {
    try {
      const servers = await Server.findAll();
      // res.render('videos/googledrive', {
      //   servers,
      //   username: req.session.user.username,
      //   userid: req.session.user.id,
      //   apiKey: req.session.user.apiKey,
      // });
      return res.status(200).json({
        servers,
        username: req.session.user.username,
        userid: req.session.user.id,
        apiKey: req.session.user.apiKey,
      });
    } catch (err) {
      res.status(500).send("Error fetching video data");
    }
  }
);

// Route for adding a single Google Drive link
router.post(
  "/api/add-single-drive",
  isAuthenticated,
  [
    check("driveLink").isURL().withMessage("Invalid URL"),
    check("serverId").notEmpty().withMessage("Server ID is required"),
    check("iMemberID").isInt().withMessage("Member ID must be an integer"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ success: false, errors: errors.array() });
    }

    try {
      const { driveLink, serverId, iMemberID } = req.body;

      // Extract file ID from Google Drive link
      const regex =
        /^https?:\/\/(?:www\.)?(?:drive|docs)\.google\.com\/(?:file\/d\/|open\?id=)?([a-zA-Z0-9_-]+)(?:\/.+)?$/;
      const match = driveLink.match(regex);
      if (!match) {
        return res.status(400).send("Invalid Google Drive link");
      }
      const fileId = match[1];

      // Generate MD5 hash of the file ID
      const hashmd5id = crypto.createHash("md5").update(fileId).digest("hex");
      const drivefull = `https://drive.google.com/file/d/${fileId}/view`;
      const timeadd = new Date();
      const timeupdate = new Date();

      // Check if the link already exists
      const existingVideo = await Video.findOne({ where: { md5: hashmd5id } });

      if (existingVideo) {
        return res
          .status(400)
          .send("This Google Drive link already exists in the database");
      }

      // Insert the new video data
      await Video.create({
        strServer: serverId,
        iMemberID,
        timeadd,
        timeupdate,
        md5: hashmd5id,
        iType: "google-drive",
        strUrl: drivefull,
      });

      // ลบแคชที่เกี่ยวข้อง
      await clearCache("search-videos");
      await clearCache("remote-google-drive");
      res.status(200).send("Video data inserted successfully");
    } catch (err) {
      console.error("Error inserting video data:", err);
      res.status(500).send("Error inserting video data");
    }
  }
);

// Route for adding a Google Drive link via API
router.get(
  "/api/add-drive",
  [
    query("url").isURL().withMessage("URL ไม่ถูกต้อง"),
    query("key")
      .notEmpty()
      .withMessage("API key จำเป็น")
      .matches(/^[a-zA-Z0-9]+$/)
      .withMessage("API key ต้องเป็นตัวอักษรหรือตัวเลขเท่านั้น"),
    query("server")
      .notEmpty()
      .withMessage("Server จำเป็น")
      .isInt({ min: 1, max: 1000 })
      .withMessage("Fuk HACKER Little BOY"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ success: false, errors: errors.array() });
    }

    try {
      const { url, key, server } = req.query;

      // ตรวจสอบว่า key ถูกต้องหรือไม่
      const user = await User.findOne({ where: { api_key: key } });

      if (!user) {
        return res.status(400).json({ error: "API key ไม่ถูกต้อง" });
      }

      const iMemberID = user.id;

      // ดึง file ID จากลิงก์ Google Drive
      const regex =
        /^https?:\/\/(?:www\.)?(?:drive|docs)\.google\.com\/(?:file\/d\/|open\?id=)?([a-zA-Z0-9_-]+)(?:\/.+)?$/;
      const match = url.match(regex);
      if (!match) {
        return res.status(400).json({ error: "ลิงก์ Google Drive ไม่ถูกต้อง" });
      }
      const fileId = match[1];

      // สร้าง MD5 hash จาก file ID
      const hashmd5id = crypto.createHash("md5").update(fileId).digest("hex");
      const drivefull = `https://drive.google.com/file/d/${fileId}/view`;
      const timeadd = new Date();
      const timeupdate = new Date();

      // ตรวจสอบว่าลิงก์มีอยู่แล้วหรือไม่
      const existingVideo = await Video.findOne({ where: { md5: hashmd5id } });

      if (existingVideo) {
        // ส่งข้อมูลที่มีอยู่แล้วกลับไป
        return res.status(200).json({
          message: "ลิงก์ Google Drive นี้มีอยู่แล้วในฐานข้อมูล",
          data: {
            id: existingVideo.id,
            name: existingVideo.strTitle,
            Url: existingVideo.strUrl,
            embed: `${process.env.BaseUrl}/embed/${existingVideo.md5}`,
            m3u8: `${process.env.BaseUrl}/hls/${existingVideo.md5}/master.m3u8`,
            Type: existingVideo.iType,
            status: existingVideo.iStatus,
          },
        });
      }

      // กำหนด iStatus ถ้าจำเป็น เช่น:
      const iStatus = "0"; // คุณอาจต้องกำหนดค่าสถานะที่เหมาะสม

      // เพิ่มข้อมูลวิดีโอใหม่
      const newVideo = await Video.create({
        strServer: server,
        iMemberID,
        timeadd,
        timeupdate,
        md5: hashmd5id,
        iType: "google-drive",
        strUrl: drivefull,
      });

      // ลบแคชที่เกี่ยวข้อง
      await clearCache("search-videos");
      await clearCache("remote-google-drive");
      res.status(200).json({
        message: "เพิ่มข้อมูลวิดีโอเรียบร้อยแล้ว",
        data: {
          id: newVideo.id,
          Url: drivefull,
          embed: `${process.env.BaseUrl}/embed/${hashmd5id}`,
          m3u8: `${process.env.BaseUrl}/hls/${hashmd5id}/master.m3u8`,
          iType: "google-drive",
          status: iStatus,
        },
      });
    } catch (err) {
      console.error("Error inserting video data:", err);
      res.status(500).json({ error: "เกิดข้อผิดพลาดในการเพิ่มข้อมูลวิดีโอ" });
    }
  }
);

module.exports = router;
