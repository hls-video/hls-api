// server/index.js

require("dotenv").config();
const express = require("express");
const router = express.Router();
const axios = require("axios");
const { Server } = require("../../models"); // นำเข้าโมเดลจาก Sequelize

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  if (tokenCookie) {
    req.session.user = {
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// Server List
router.get("/server", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    // res.render('server/index', {
    //   servers,
    //   username: req.session.user.username,
    //   apiKey: req.session.user.apiKey,
    // });
    return res.status(200).json({
      servers,
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Error fetching server data");
  }
});

// Server ADD
router.get("/server-add", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    res.render("server/add", {
      servers,
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
    });
  } catch (err) {
    res.status(500).send("Error fetching video data");
  }
});

// Server ADD post
router.post("/server-add", isAuthenticated, async (req, res) => {
  const { name, ip, domains, domainupload, upload } = req.body;
  const status = 1; // Assuming status 1 means active
  const domainsup = domainupload;
  try {
    // Fetch HDD status from the API
    const response = await axios.get(`http://${ip}/api/hdd/status`);
    const { free, total } = response.data;

    const TotalStorage = `${(total / 1024 ** 3).toFixed(2)}GB`; // Convert bytes to GB and format to 2 decimal places
    const UseStorage = `${((total - free) / 1024 ** 3).toFixed(2)}GB`; // Convert bytes to GB and format to 2 decimal places

    // แปลง domains เป็น JSON string
    const domainsString = JSON.stringify(domains);

    await Server.create({
      name,
      ip,
      domains: domainsString,
      status,
      domainsup,
      upload,
      TotalStorage,
      UseStorage,
    });

    res.status(200).json({
      success: true,
      message: "Server added successfully",
      TotalStorage,
      UseStorage,
    });
  } catch (err) {
    console.error("Error adding server:", err);
    res.status(500).json({ success: false, message: "Error adding server" });
  }
});

// Server refresh post
router.post("/server-refresh", isAuthenticated, async (req, res) => {
  const { ip } = req.body;

  try {
    // Fetch HDD status from the API
    const response = await axios.get(`http://${ip}/api/hdd/status`);
    const { free, total } = response.data;

    const TotalStorage = `${(total / 1024 ** 3).toFixed(2)}GB`; // Convert bytes to GB and format to 2 decimal places
    const UseStorage = `${((total - free) / 1024 ** 3).toFixed(2)}GB`; // Convert bytes to GB and format to 2 decimal places

    // Update the server storage info in the database
    await Server.update({ TotalStorage, UseStorage }, { where: { ip } });

    res.status(200).json({
      success: true,
      message: "Server updated successfully",
      TotalStorage,
      UseStorage,
    });
  } catch (err) {
    console.error("Error updating server:", err);
    res.status(500).json({ success: false, message: "Error updating server" });
  }
});

// Server Edit
router.get("/server-edit", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    res.render("server/edit", {
      servers,
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
    });
  } catch (err) {
    res.status(500).send("Error fetching video data");
  }
});

// Server Logs
router.get("/server-logs", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    res.render("server/logs", {
      servers,
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
    });
  } catch (err) {
    res.status(500).send("Error fetching video data");
  }
});

// Delete server route
router.delete("/server/:id", isAuthenticated, async (req, res) => {
  try {
    const serverId = req.params.id;
    await Server.destroy({ where: { id: serverId } });
    res.status(200).send({ message: "Server deleted successfully" });
  } catch (error) {
    console.error("Error deleting server:", error);
    res.status(500).send({ message: "Failed to delete the server", error });
  }
});

// Update server route
router.put("/server/:id", isAuthenticated, async (req, res) => {
  const { id } = req.params;
  const { name, ip, domains, domainsup, upload } = req.body;

  try {
    // No need to convert domains to JSON string since it's already in JSON string format
    await Server.update(
      { name, ip, domains, domainsup, upload },
      { where: { id } }
    );

    res.send({ message: "Server details updated successfully" });
  } catch (error) {
    console.error("Error updating server:", error);
    res.status(500).send({ message: "Failed to update the server", error });
  }
});

module.exports = router;
