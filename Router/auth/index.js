const { sequelize, User } = require("../../models");
const express = require("express");
const bcrypt = require("bcrypt");
const session = require("express-session");
const { body, validationResult, cookie } = require("express-validator");
const router = express.Router();
const saltRounds = 10;
const crypto = require("crypto");

function generateNewToken() {
  return crypto.randomBytes(32).toString("hex");
}

// ตั้งค่า session
router.use(
  session({
    secret: "your_secret_key",
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 24 * 60 * 60 * 1000 }, // กำหนดอายุ session 1 วัน
  })
);

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  if (req.session.user) {
    return next();
  } else {
    res.redirect("/auth/login");
  }
}

// Login route
router.get("/login", (req, res) => {
  res.render("auth/login", { message: req.flash("error") });
});

// Register route
router.get("/register", (req, res) => {
  res.render("auth/register");
});

// Register handler
router.post(
  "/register",
  [
    body("username").isString().notEmpty().withMessage("Username is required"),
    body("password")
      .isLength({ min: 4 })
      .withMessage("Password must be at least 6 characters long"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, password } = req.body;

    try {
      // ตรวจสอบว่าผู้ใช้ซ้ำหรือไม่
      const userCheck = await User.findOne({ where: { username } });
      if (userCheck) {
        return res
          .status(201)
          .json({ success: false, message: "Username already exists" });
      }

      const hash = await bcrypt.hash(password, saltRounds);
      const apiKey = generateNewToken();

      const newUser = await User.create({
        username,
        password: hash,
        api_key: apiKey,
      });
      res
        .status(200)
        .send({ success: true, message: "User registered successfully" });
    } catch (err) {
      console.error("Error registering user:", err); // แสดงข้อผิดพลาดใน console
      res.status(500).send({
        success: false,
        message: "Error registering user",
        error: err.message,
      });
    }
  }
);

// Login handler
router.post(
  "/login",
  [
    body("username").isString().notEmpty().withMessage("Username is required"),
    body("password").isString().notEmpty().withMessage("Password is required"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, password } = req.body;

    try {
      const user = await User.findOne({ where: { username } });

      if (!user) {
        return res
          .status(401)
          .json({ success: false, message: "Invalid username or password" });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        return res
          .status(401)
          .json({ success: false, message: "Invalid username or password" });
      }

      // req.session.user = user;
      // req.session.apiKey = user.api_key;
      // req.session.tokenExpiry = new Date(Date.now() + 24 * 60 * 60 * 1000); // Set expiration to 1 day from now

      let expiry = new Date(Date.now() + 24 * 60 * 60 * 1000);
      res.cookie("token", user.api_key, { maxAge: expiry, httpOnly: true });
      res.cookie("username", user.username, { maxAge: expiry, httpOnly: true });

      let userCookie = [`username=${user.username}`, `path=/`];
      let idCookie = [`id=${user.id}`, `path=/`];
      let tokenCookie = [`token=${user.api_key}`, `path=/`];
      res.status(200).json({
        success: true,
        data: user,
        cookie: {
          username: userCookie.join("; "),
          token: tokenCookie.join("; "),
          id: idCookie.join("; "),
        },
      });
    } catch (err) {
      console.error("Error logging in:", err); // แสดงข้อผิดพลาดใน console
      res.status(500).json({
        success: false,
        message: "Error logging in",
        error: err.message,
      });
    }
  }
);

// Dashboard route (protected)
router.get("/dashboard", isAuthenticated, (req, res) => {
  res.render("dashboard", { user: req.session.user });
});

// Logout route
router.get("/logout", (req, res) => {
  res.clearCookie("id");
  res.clearCookie("username");
  res.clearCookie("token");
  req.session.destroy((err) => {
    if (err) {
      return res.status(500).send("Error logging out");
    }
  });
  res.status(200).send("Logged out successfully");
});

module.exports = router;
