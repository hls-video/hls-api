const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");
const { User, Video, Server } = require("../../models"); // นำเข้าโมเดลจาก Sequelize

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  if (tokenCookie) {
    req.session.username = usernameCookie;
    req.session.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// Function to format size
function formatSize(size) {
  if (isNaN(size)) {
    return "0 Bytes";
  }
  const units = ["Bytes", "KB", "MB", "GB", "TB"];
  let unitIndex = 0;
  while (size >= 1024 && unitIndex < units.length - 1) {
    size /= 1024;
    unitIndex++;
  }
  return `${size.toFixed(2)} ${units[unitIndex]}`;
}

router.get("/dashboard", isAuthenticated, async (req, res) => {
  try {
    const iMemberID = req.session.user.id;

    // Query for video process statuses
    const totalVideos = await Video.count({ where: { iMemberID } });
    const completedVideos = await Video.count({
      where: { iMemberID, iStatus: 1 },
    });
    const failedVideos = await Video.count({
      where: { iMemberID, iStatus: 2 },
    });
    const waitingVideos = await Video.count({
      where: { iMemberID, iStatus: 0 },
    });
    const downloadingVideos = await Video.count({
      where: { iMemberID, iStatus: 4 },
    });
    const encodingVideos = await Video.count({
      where: { iMemberID, iStatus: 3 },
    });

    // Query for server data
    const servers = await Server.findAll();

    // Prepare server spaces
    const serverSpaces = await Promise.all(
      servers.map(async (server) => {
        const db_strServer = server.id;
        const processCount1 = await Video.count({
          where: { iStatus: 0, strServer: db_strServer },
        });
        const processCount2 = await Video.count({
          where: { iStatus: 3, strServer: db_strServer },
        });
        const processCount3 = await Video.count({
          where: { iStatus: 2, strServer: db_strServer },
        });
        const processCount4 = await Video.count({
          where: { iStatus: 1, strServer: db_strServer },
        });
        const totalSizeResult = await Video.sum("TotalSize", {
          where: { strServer: db_strServer },
        });

        const totalSize = totalSizeResult || 0;

        return {
          id: server.id,
          serverName: server.name,
          totalSize: formatSize(Number(totalSize)),
          totalVideos:
            processCount1 + processCount2 + processCount3 + processCount4,
          completedVideos: processCount4,
          failedVideos: processCount3,
          waitingVideos: processCount1,
          downloadingVideos: processCount2,
          encodingVideos: processCount2, // สมมติว่า encoding videos อยู่ใน processCount2
          domains: JSON.parse(server.domains), // แปลง domains เป็น JSON
        };
      })
    );

    // res.render("dashboard/index", {
    //   username: req.session.user.username,
    //   apiKey: req.session.apiKey,
    //   totalVideos,
    //   completedVideos,
    //   failedVideos,
    //   waitingVideos,
    //   downloadingVideos,
    //   encodingVideos,
    //   servers,
    //   serverSpaces,
    // });
    res.status(200).json({
      username: req.session.user.username,
      apiKey: req.session.apiKey,
      totalVideos,
      completedVideos,
      failedVideos,
      waitingVideos,
      downloadingVideos,
      encodingVideos,
      servers,
      serverSpaces,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Error fetching dashboard data");
  }
});

const countFilesAndFolders = (dir) => {
  return new Promise((resolve, reject) => {
    fs.readdir(dir, (err, files) => {
      if (err) return reject(err);
      let fileCount = 0;
      let folderCount = 0;
      files.forEach((file) => {
        const filePath = path.join(dir, file);
        if (fs.lstatSync(filePath).isDirectory()) {
          folderCount++;
        } else {
          fileCount++;
        }
      });
      resolve({ fileCount, folderCount });
    });
  });
};

router.post("/cache/check-caches", async (req, res) => {
  const projectRoot = path.join(__dirname, "..", "..");
  const cacheDir = path.join(projectRoot, "cache");
  const filesDir = path.join(projectRoot, "files");

  const checkDirectoryExists = (dir) => {
    return new Promise((resolve, reject) => {
      fs.access(dir, fs.constants.F_OK, (err) => {
        if (err) {
          return reject(new Error(`Directory not found: ${dir}`));
        }
        resolve(true);
      });
    });
  };

  try {
    await checkDirectoryExists(cacheDir);
    await checkDirectoryExists(filesDir);

    const cacheCount = await countFilesAndFolders(cacheDir);
    const filesCount = await countFilesAndFolders(filesDir);

    if (
      cacheCount.fileCount === 0 &&
      cacheCount.folderCount === 0 &&
      filesCount.fileCount === 0 &&
      filesCount.folderCount === 0
    ) {
      return res
        .status(200)
        .json({ message: "โฟลเดอร์ว่างเปล่าหรือข้อมูลถูกลบไปหมดแล้ว" });
    }

    res.json({
      cache: cacheCount,
      files: filesCount,
    });
  } catch (error) {
    res
      .status(500)
      .json({ message: `Error checking caches: ${error.message}` });
  }
});

router.post("/cache/delete-caches", (req, res) => {
  const projectRoot = path.join(__dirname, "..", "..");
  const cacheDir = path.join(projectRoot, "cache");
  const filesDir = path.join(projectRoot, "files");

  const deleteFilesInDirectory = (dir, callback) => {
    fs.readdir(dir, (err, files) => {
      if (err) {
        console.error(`ไม่สามารถอ่านไดเรกทอรี: ${dir}`, err);
        return callback(err);
      }

      let fileCount = files.length;
      if (fileCount === 0) {
        return callback(null);
      }

      files.forEach((file) => {
        const filePath = path.join(dir, file);
        fs.lstat(filePath, (err, stats) => {
          if (err) {
            console.error(`ไม่สามารถดึงสถิติของไฟล์: ${filePath}`, err);
            return callback(err);
          }

          if (stats.isDirectory()) {
            deleteFilesInDirectory(filePath, (err) => {
              if (err) {
                return callback(err);
              }
              fs.rmdir(filePath, (err) => {
                if (err) {
                  console.error(`ไม่สามารถลบไดเรกทอรีย่อย: ${filePath}`, err);
                  return callback(err);
                }
                fileCount -= 1;
                if (fileCount === 0) {
                  callback(null);
                }
              });
            });
          } else {
            fs.unlink(filePath, (err) => {
              if (err) {
                console.error(`ไม่สามารถลบไฟล์: ${filePath}`, err);
                return callback(err);
              }

              fileCount -= 1;
              if (fileCount === 0) {
                callback(null);
              }
            });
          }
        });
      });
    });
  };

  deleteFilesInDirectory(cacheDir, (err) => {
    if (err) {
      return res.status(500).send("ไม่สามารถลบไฟล์แคชได้");
    }

    deleteFilesInDirectory(filesDir, (err) => {
      if (err) {
        return res.status(500).send("ไม่สามารถลบไฟล์ได้");
      }

      res.send("ลบแคชเรียบร้อย");
    });
  });
});

router.get("/profile", isAuthenticated, (req, res) => {
  // res.render('dashboard/profile', {
  //   username: req.session.user.username,
  //   apiKey: req.session.apiKey,
  // });
  return res.status(200).json({
    username: req.session.username,
    apiKey: req.session.apiKey,
  });
});

router.post("/reset-errors", isAuthenticated, async (req, res) => {
  try {
    // อัพเดต iStatus ของวิดีโอที่มี iStatus เท่ากับ 2 เป็น 0
    const [numberOfAffectedRows] = await Video.update(
      { iStatus: 0 },
      { where: { iStatus: 2 } }
    );

    return res
      .status(200)
      .json({ message: `Updated ${numberOfAffectedRows} videos' status to 0` });
  } catch (error) {
    console.error("Error resetting video status:", error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

router.post("/reset-errors/:id", isAuthenticated, async (req, res) => {
  try {
    // อัพเดต iStatus ของวิดีโอที่มี iStatus เท่ากับ 2 เป็น 0 ของ แต่ละ Server
    const [numberOfAffectedRows] = await Video.update(
      { iStatus: 0 },
      { where: { iStatus: 2 } }
    );

    return res
      .status(200)
      .json({ message: `Updated ${numberOfAffectedRows} videos' status to 0` });
  } catch (error) {
    console.error("Error resetting video status:", error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

router.post("/reset-errors/:id", isAuthenticated, async (req, res) => {
  try {
    // อัพเดต iStatus ของวิดีโอที่มี iStatus เท่ากับ 2 เป็น 0 ของแต่ละ Server
    const [numberOfAffectedRows] = await Video.update(
      { iStatus: 0 },
      { where: { iStatus: 2, strServer: req.params.id } }
    );

    return res
      .status(200)
      .json({ message: `Updated ${numberOfAffectedRows} videos' status to 0` });
  } catch (error) {
    console.error("Error resetting video status:", error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

module.exports = router;
