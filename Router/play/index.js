require("dotenv").config();
const express = require("express");
const router = express.Router();
const axios = require("axios");
const {
  Video,
  Server,
  Domain,
  AdField,
  PlayerSettings,
} = require("../../models"); // Import Sequelize models
const path = require("path");
const fs = require("fs").promises;
const fsSync = require("fs"); // Synchronous fs module
const apicache = require("apicache");

const cache = apicache.middleware;

// มิดเดิลแวร์แคชแบบกำหนดเองเพื่อรวมเวลาแคชที่เหลือใน Headers
const customCache = (duration) => {
  return (req, res, next) => {
    const cachingMiddleware = cache(duration);
    cachingMiddleware(req, res, (err) => {
      if (!err) {
        const cacheEntry = apicache.getIndex().all[req.originalUrl];
        if (cacheEntry) {
          const remainingTime = Math.max(
            0,
            (cacheEntry.duration - (Date.now() - cacheEntry.timestamp)) / 1000
          );
          res.setHeader(
            "X-Cache-Remaining",
            `${remainingTime.toFixed(0)} seconds`
          );
        }
      }
      next(err);
    });
  };
};
const iUpload = (service) => {
  const services = {
    localhost: {
      m3u8: (video, serverthumbnail) =>
        `${process.env.BaseUrl}/hls/${video.md5}/master.m3u8`,
      m3u8txt: (video) => `${process.env.BaseUrl}/hls/${video.md5}/master`,
      thumbnail: (video, serverthumbnail) =>
        `//${serverthumbnail[0]}/files/${video.md5}/thumbnail.jpg`,
    },
    ftp: {
      m3u8: (video, serverthumbnail) =>
        `${process.env.BaseUrl}/hlsftp/${video.md5}/master.m3u8`,
      m3u8txt: (video) => `${process.env.BaseUrl}/hlsftp/${video.md5}/master`,
      thumbnail: (video, serverthumbnail) =>
        `//${serverthumbnail[0]}/files/${video.md5}/thumbnail.jpg`,
    },
    r2: {
      m3u8: (video, serverthumbnail) =>
        `${process.env.BaseUrl}/hlsr2/${video.md5}/master.m3u8`,
      m3u8txt: (video) => `${process.env.BaseUrl}/hlsr2/${video.md5}/master`,
      thumbnail: (video, serverthumbnail) =>
        `//${serverthumbnail[0]}/files%2F${video.md5}%2Fthumbnail.jpg`,
    },
    s3: {
      m3u8: (video, serverthumbnail) =>
        `${process.env.BaseUrl}/hlss3/${video.md5}/master.m3u8`,
      m3u8txt: (video) => `${process.env.BaseUrl}/hlss3/${video.md5}/master`,
      thumbnail: (video, serverthumbnail) =>
        `//${serverthumbnail[0]}/filess3/${video.md5}/thumbnail.jpg`,
    },
    // เพิ่มบริการอื่นๆ ในรูปแบบเดียวกัน
    // ...
    sftp: {
      m3u8: (video, serverthumbnail) =>
        `${process.env.BaseUrl}/hlssftp/${video.md5}/master.m3u8`,
      m3u8txt: (video) => `${process.env.BaseUrl}/hlssftp/${video.md5}/master`,
      thumbnail: (video, serverthumbnail) =>
        `//${serverthumbnail[0]}/filessftp/${video.md5}/thumbnail.jpg`,
    },
  };

  return services[service] || null;
};

router.get("/play/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const playerSettings = await PlayerSettings.findOne();
    if (!playerSettings) {
      return res.status(500).send("Player settings not found");
    }

    const video = await Video.findOne({ where: { md5: id } });

    if (!video) {
      return res.status(404).send("ไม่พบวิดีโอ");
    }

    const uploadService = iUpload(video.iUpload);
    if (!uploadService) {
      return res.status(400).send("บริการที่ใช้ในการอัปโหลดไม่ถูกต้อง");
    }

    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).send("ไม่พบเซิร์ฟเวอร์");
    }

    const serverthumbnail = JSON.parse(server.domains);

    const m3u8 = uploadService.m3u8(video, serverthumbnail);
    const m3u8txt = uploadService.m3u8txt(video);
    const thumbnail = uploadService.thumbnail(video, serverthumbnail);

    // ดึงข้อมูลโดเมนและฟิลด์โฆษณาที่เกี่ยวข้อง
    const domains = await Domain.findAll({
      include: [{ model: AdField, as: "adFields" }],
    });

    // จัดรูปแบบโดเมนและฟิลด์โฆษณาให้ง่ายต่อการใช้งานในเทมเพลต
    const formattedDomains = domains.map((domain) => ({
      id: domain.id,
      domainName: domain.domainName,
      defaultBgImage: domain.defaultBgImage,
      hideLogo: domain.hideLogo,
      logoURL: domain.logoURL,
      logoLink: domain.logoLink,
      logoPosition: domain.logoPosition,
      adActive: domain.adActive,
      adFields: domain.adFields.map((adField) => ({
        adURL: adField.adURL,
        adLink: adField.adLink,
        adTime: adField.adTime,
      })), // ตรวจสอบให้แน่ใจว่า adFields เป็นอาเรย์ของออบเจ็กต์ที่มีฟิลด์ที่ต้องการ
    }));

    // ดึงข้อมูลโดเมนจากหัว Referer
    const referer = req.get("Referer");
    let refererDomain = "";

    if (referer) {
      try {
        const url = new URL(referer);
        refererDomain = url.hostname;
      } catch (err) {
        console.error("URL Referer ไม่ถูกต้อง:", referer, err);
      }
    }

    // ตรวจสอบว่ามีโดเมนในรายการทั้งหมดที่ตรงกับ refererDomain หรือ subdomain หรือไม่
    const matchingDomain = formattedDomains.find((domain) => {
      const domainName = domain.domainName;
      return (
        refererDomain === domainName || refererDomain.endsWith(`.${domainName}`)
      );
    });

    let allowedSites = playerSettings.allowedSites;
    if (allowedSites && allowedSites.trim() !== "") {
      const acceptedDomains = allowedSites.split(",");

      if (
        !refererDomain ||
        !acceptedDomains.some(
          (domain) =>
            refererDomain === domain || refererDomain.endsWith(`.${domain}`)
        )
      ) {
        return res.status(403).send(`
           <!DOCTYPE html>
           <html lang="en">
           <head>
             <meta charset="utf-8">
             <title>Error</title>
             <style>
               body,html{width:100%;height:100%;margin:0;padding:0;background-color:#151515;overflow:hidden}
               body{text-align:center;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;color:#fff}
               #horizon{display:table;height:100%;position:static;width:100%}
               #sun{display:table-cell;vertical-align:middle;position:static;margin:0 auto}
               a,h1,p{margin:0 auto;max-width:80%}
               h1,p{display:block}
               h1{font-size:48px;margin-bottom:5px}
               p{font-size:24px;max-width:475px}
               a{color:#4bf;text-decoration:none}
               a[role=button]{margin-top:15px;display:inline-block;color:#fff;font-weight:700;padding:10px 20px;border-radius:5px;background:#4bf;text-shadow:none;font-size:16px}
               a[role=button]:active{background:#3795cc}
               .smaller p{font-size:18px}
               .smaller h1{font-size:42px}
               @media (max-width:499px){p{font-size:16px;line-height:1.4;max-width:290px}
               a[role=button]{font-size:16px;padding:7px 15px}
               .smaller p{font-size:15px}
               .smaller h1{font-size:36px}}
               @media (max-width:300px){h1{font-size:36px}
               p{font-size:14px;line-height:1.4;max-width:250px}
               a[role=button]{margin-top:10px;font-size:14px}
               .smaller p{font-size:12px}
               .smaller a[role=button]{font-size:12px}
               .smaller h1{font-size:28px}}
             </style>
           </head>
           <body>
             <div id="horizon">
               <div id="sun">
                 <h1>This web page is unavailable.</h1>
                 <p>ERROR</p>
               </div>
             </div>
           </body>
           </html>
         `);
      }
    }

    // กรองโดเมนที่ใช้งาน
    const activeDomains = formattedDomains.filter(
      (domain) => domain.adActive === true
    );

    // สร้าง adsx จาก adFields โดยตรวจสอบ matchingDomain ก่อน และถ้าไม่มีให้ใช้ activeDomains
    let adsx = [];
    if (matchingDomain) {
      if (activeDomains.length > 0) {
        adsx = matchingDomain.adFields.map((adField) => ({
          image: matchingDomain.defaultBgImage || thumbnail,
          sources: [
            {
              file: adField.adURL,
              skiptime: adField.adTime,
              clickUrl: adField.adLink,
            },
          ],
        }));
      } else {
        adsx = [];
      }
    } else if (activeDomains.length > 0) {
      adsx = activeDomains.flatMap((domain) =>
        domain.adFields.map((adField) => ({
          image: domain.defaultBgImage || thumbnail,
          sources: [
            {
              file: adField.adURL,
              skiptime: adField.adTime,
              clickUrl: adField.adLink,
            },
          ],
        }))
      );
    }

    // ดึงข้อมูลโลโก้จากโดเมนที่ตรงกันหรือโดเมนที่ใช้งานแรก, หรือใช้ค่าเริ่มต้นถ้าไม่มีโดเมนที่ใช้งาน
    const logoInfo =
      matchingDomain ||
      (activeDomains.length > 0
        ? activeDomains[0]
        : { logoURL: "", logoLink: "", logoPosition: "" });
    const logoHide = logoInfo.hideLogo === "" ? true : false;

    // res.render("play/ads", {
    //   videoTitle: video.strTitle,
    //   videomd5: video.md5,
    //   videostrQualities: video.strQualities,
    //   videoServer: video.strServer,
    //   m3u8,
    //   m3u8txt,
    //   thumbnail,
    //   logoURL: logoInfo.logoURL,
    //   logoLink: logoInfo.logoLink,
    //   logoPosition: logoInfo.logoPosition,
    //   adsx: JSON.stringify(adsx),
    // });
    return res.status(200).json({
      videoTitle: video.strTitle,
      videomd5: video.md5,
      videostrQualities: video.strQualities,
      videoServer: video.strServer,
      m3u8,
      m3u8txt,
      thumbnail,
      logoURL: logoInfo.logoURL,
      logoLink: logoInfo.logoLink,
      logoPosition: logoInfo.logoPosition,
      adsx: JSON.stringify(adsx),
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("เกิดข้อผิดพลาดในการดึงข้อมูลวิดีโอ");
  }
});

// เส้นทางเล่นวิดีโอ
router.get("/playx/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const video = await Video.findOne({ where: { md5: id } });

    if (!video) {
      return res.status(404).send("ไม่พบวิดีโอ");
    }

    const uploadService = iUpload(video.iUpload);
    if (!uploadService) {
      return res.status(400).send("บริการที่ใช้ในการอัปโหลดไม่ถูกต้อง");
    }

    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).send("ไม่พบเซิร์ฟเวอร์");
    }

    const serverthumbnail = JSON.parse(server.domains);

    const m3u8 = uploadService.m3u8(video, serverthumbnail);
    const m3u8txt = uploadService.m3u8txt(video);
    const thumbnail = uploadService.thumbnail(video, serverthumbnail);

    // res.render('play/index', {
    //   videoTitle: video.strTitle,
    //   videomd5: video.md5,
    //   videostrQualities: video.strQualities,
    //   videoServer: video.strServer,
    //   m3u8,
    //   m3u8txt,
    //   thumbnail,
    // });
    return res.status(200).json({
      videoTitle: video.strTitle,
      videomd5: video.md5,
      videostrQualities: video.strQualities,
      videoServer: video.strServer,
      m3u8,
      m3u8txt,
      thumbnail,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("เกิดข้อผิดพลาดในการดึงข้อมูลวิดีโอ");
  }
});

// Helper function to ensure directory existence
const ensureDirectoryExistence = async (filePath) => {
  const dirname = path.dirname(filePath);
  try {
    await fs.mkdir(dirname, { recursive: true });
  } catch (err) {
    if (err.code !== "EEXIST") {
      throw err;
    }
  }
};

// Helper function to check if cache file is expired
const isCacheExpired = async (filePath, ttlInSeconds) => {
  try {
    const stats = await fs.stat(filePath);
    const now = new Date().getTime();
    const fileTime = new Date(stats.mtime).getTime();
    return now - fileTime > ttlInSeconds * 1000;
  } catch (err) {
    // If file doesn't exist or another error occurs, consider cache as expired
    return true;
  }
};

router.get(["/hls/:id/master", "/hls/:id/master.m3u8"], async (req, res) => {
  const { id } = req.params;

  try {
    const video = await Video.findOne({ where: { md5: id } });
    if (!video) {
      return res.status(404).send("Video not found");
    }

    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).send("Server not found");
    }

    let qualities;
    try {
      qualities = JSON.parse(video.strQualities);
    } catch (parseError) {
      return res
        .status(500)
        .json({ message: "Error processing video qualities." });
    }

    const dirPath = path.join(__dirname, `../../files/${video.md5}`);
    const filePath = path.join(dirPath, "playlist.txt");
    const cacheFilePath = path.join(dirPath, "playlist.cache");

    const cacheTTL = 3600; // 1 hour in seconds

    if (!(await isCacheExpired(cacheFilePath, cacheTTL))) {
      // Cache file exists and is not expired
      const cacheContent = await fs.readFile(cacheFilePath, "utf8");
      res.type("text/plain");
      return res.send(cacheContent);
    }

    if (
      await fs
        .access(filePath)
        .then(() => true)
        .catch(() => false)
    ) {
      let playlistContent = await fs.readFile(filePath, "utf8");

      // Update audio URLs
      for (let i = 0; i <= 10; i++) {
        const regexPattern = new RegExp(`audio/audio_${i}\\.m3u8`, "g");
        playlistContent = playlistContent.replace(regexPattern, () => {
          return `${process.env.BaseUrl}/files/${video.md5}/audio/audio_${i}`;
        });
      }

      qualities.forEach((resolution) => {
        const regexPattern = new RegExp(`${resolution}/index\\.m3u8`, "g");
        playlistContent = playlistContent.replace(regexPattern, () => {
          return `${process.env.BaseUrl}/files/${video.md5}/${resolution}/index`;
        });
      });

      // Cache the playlist content
      await fs.writeFile(cacheFilePath, playlistContent);
      res.type("text/plain");
      return res.send(playlistContent);
    }

    const ipAddress = server.ip;
    const playlistUrl = `http://${ipAddress}/files/${video.md5}/playerlist.m3u8`;

    const response = await axios.get(playlistUrl);

    await ensureDirectoryExistence(filePath);

    let updatedPlaylist = response.data;

    // Function to count audio segments
    function countAudioSegments(playlistContent) {
      const audioSegmentPattern = /#EXT-X-MEDIA:TYPE=AUDIO/g;
      const segments = playlistContent.match(audioSegmentPattern);
      const segmentCount = segments ? segments.length : 0;
      return segmentCount - 1;
    }
    const audioSegmentCount = countAudioSegments(updatedPlaylist);

    // Update audio URLs
    for (let i = 0; i <= audioSegmentCount; i++) {
      const audioFilePath = path.join(dirPath, "audio", `audio_${i}.txt`);
      const audioURL = `http://${ipAddress}/files/${video.md5}/audio/audio_${i}.m3u8`;
      try {
        const res = await axios.get(audioURL);
        await ensureDirectoryExistence(audioFilePath);
        await fs.writeFile(audioFilePath, res.data);
      } catch (error) {
        return res
          .status(500)
          .json({ message: "Wait for the video to be processed." });
      }

      const regexPattern = new RegExp(`audio/audio_${i}\\.m3u8`, "g");
      updatedPlaylist = updatedPlaylist.replace(regexPattern, () => {
        return `${process.env.BaseUrl}/files/${video.md5}/audio/audio_${i}`;
      });
    }

    await Promise.all(
      qualities.map(async (resolution) => {
        const resolutionWithoutP = resolution;
        const regexPattern = new RegExp(
          `${resolutionWithoutP}/index.m3u8`,
          "g"
        );
        updatedPlaylist = updatedPlaylist.replace(regexPattern, (match) => {
          return `${process.env.BaseUrl}/files/${video.md5}/${resolutionWithoutP}/index`;
        });

        const resolutionURL = `http://${ipAddress}/files/${video.md5}/${resolutionWithoutP}/index.m3u8`;
        try {
          const res = await axios.get(resolutionURL);
          const resolutionFilePath = path.join(
            dirPath,
            resolutionWithoutP,
            "index.txt"
          );
          await ensureDirectoryExistence(resolutionFilePath);
          await fs.writeFile(resolutionFilePath, res.data);
        } catch (error) {
          return res
            .status(500)
            .json({ message: "Wait for the video to be processed." });
        }
      })
    );

    await fs.writeFile(filePath, updatedPlaylist);
    res.type("text/plain");
    res.send(updatedPlaylist);
  } catch (error) {
    res.status(500).json({ message: "Wait for the video to be processed." });
  }
});

router.get(
  "/files/:id/:resolution/index",
  customCache("15 minutes"),
  async (req, res) => {
    try {
      const { id, resolution } = req.params;
      const cacheDir = path.join(__dirname, `../../cache/${id}/${resolution}`);
      const cacheFilePath = path.join(cacheDir, "index.txt");

      // Create the cache directory if it doesn't exist
      if (!fsSync.existsSync(cacheDir)) {
        await fs.mkdir(cacheDir, { recursive: true });
      }

      if (fsSync.existsSync(cacheFilePath)) {
        // If the cached TXT file exists, read and send it
        const cachedPlaylist = await fs.readFile(cacheFilePath, "utf8");
        res.type("text/plain").send(cachedPlaylist);
      } else {
        const video = await Video.findOne({ where: { md5: id } });
        if (!video) {
          return res.status(404).send("Video not found");
        }

        const server = await Server.findOne({ where: { id: video.strServer } });
        if (!server) {
          return res.status(404).send("Server not found");
        }

        let serverList;
        try {
          serverList = JSON.parse(server.domains);
        } catch (parseError) {
          return res
            .status(500)
            .json({ message: "Error processing video qualities." });
        }

        const dirPath = path.join(__dirname, `../../files/${id}/`);
        const filePath = `${dirPath}${resolution}/index.txt`;

        if (fsSync.existsSync(filePath)) {
          let fileContent = await fs.readFile(filePath, "utf8");
          let lines = fileContent.split("\n");

          // Remove the unwanted string from each line if present
          lines = lines.map((line) => {
            if (line.includes(",IV=0x00000000000000000000000000000000")) {
              return line.replace(",IV=0x00000000000000000000000000000000", "");
            }
            return line;
          });
          lines = lines.map((line) => {
            if (line.includes("sign.bin")) {
              return line.replace(
                "sign.bin",
                `//hlsnew.movie-117.online/sign.bin`
              );
            }
            return line;
          });

          // Rest of your existing code for processing lines
          for (let i = 0; i < lines.length; i++) {
            if (lines[i].startsWith("#EXTINF:")) {
              const tsFile = lines[i + 1];
              if (tsFile) {
                lines[i + 1] = `${tsFile}`;
              }
            }
          }

          let i = 0;
          lines.forEach((line, index) => {
            if (
              line.includes(".html") ||
              line.includes(".ts") ||
              line.includes(".aaa") ||
              line.includes(".jpg") ||
              line.includes(".png")
            ) {
              const modifiedLine = `//${
                serverList[i]
              }/files/${id}/${resolution}/${line.replace(".jpg", ".jpg")}`;
              lines[index] = modifiedLine;
              i++;
              if (i >= serverList.length) {
                i = 0;
              }
            }
          });

          const updatedPlaylist = lines.join("\n");

          // Cache the updated playlist as a TXT file
          await fs.writeFile(cacheFilePath, updatedPlaylist, "utf8");

          res.type("text/plain").send(updatedPlaylist);
        } else {
          res.status(404).json({ message: "Playlist file not found" });
        }
      }
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }
);

router.get(
  "/files/:id/audio/:audio",
  customCache("15 minutes"),
  async (req, res) => {
    try {
      const { id, audio } = req.params;
      const cacheDir = path.join(__dirname, `../../cache/${id}/audio/`);
      const cacheFilePath = path.join(cacheDir, `${audio}.txt`);

      // Create the cache directory if it doesn't exist
      if (!fsSync.existsSync(cacheDir)) {
        await fs.mkdir(cacheDir, { recursive: true });
      }

      if (fsSync.existsSync(cacheFilePath)) {
        // If the cached TXT file exists, read and send it
        const cachedPlaylist = await fs.readFile(cacheFilePath, "utf8");
        res.type("text/plain").send(cachedPlaylist);
      } else {
        const video = await Video.findOne({ where: { md5: id } });
        if (!video) {
          return res.status(404).send("Video not found");
        }

        const server = await Server.findOne({ where: { id: video.strServer } });
        if (!server) {
          return res.status(404).send("Server not found");
        }

        let serverList;
        try {
          serverList = JSON.parse(server.domains);
        } catch (parseError) {
          return res
            .status(500)
            .json({ message: "Error processing server domains." });
        }

        const dirPath = path.join(__dirname, `../../files/${id}/`);
        const filePath = `${dirPath}audio/${audio}.txt`;

        if (fsSync.existsSync(filePath)) {
          let fileContent = await fs.readFile(filePath, "utf8");
          let lines = fileContent.split("\n");

          // Remove the unwanted string from each line if present
          lines = lines.map((line) => {
            if (line.includes(",IV=0x00000000000000000000000000000000")) {
              return line.replace(",IV=0x00000000000000000000000000000000", "");
            }
            return line;
          });

          lines = lines.map((line) => {
            if (line.includes("sign.bin")) {
              return line.replace(
                "sign.bin",
                "//hlsnew.movie-117.online/sign.bin"
              );
            }
            return line;
          });

          // Rest of your existing code for processing lines
          for (let i = 0; i < lines.length; i++) {
            if (lines[i].startsWith("#EXTINF:")) {
              const tsFile = lines[i + 1];
              if (tsFile) {
                lines[i + 1] = `${tsFile}`;
              }
            }
          }

          let i = 0;
          lines.forEach((line, index) => {
            if (
              line.includes(".html") ||
              line.includes(".ts") ||
              line.includes(".aaa") ||
              line.includes(".jpg") ||
              line.includes(".png")
            ) {
              const modifiedLine = `//${
                serverList[i]
              }/files/${id}/audio/${line.replace(".jpg", ".jpg")}`;
              lines[index] = modifiedLine;
              i++;
              if (i >= serverList.length) {
                i = 0;
              }
            }
          });

          const updatedPlaylist = lines.join("\n");

          // Cache the updated playlist as a TXT file
          await fs.writeFile(cacheFilePath, updatedPlaylist, "utf8");

          res.type("text/plain").send(updatedPlaylist);
        } else {
          res.status(404).json({ message: "Playlist file not found" });
        }
      }
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }
);

module.exports = router;
