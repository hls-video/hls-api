require('dotenv').config();
const express = require('express');
const router = express.Router();
const axios = require('axios');
const { Video, Server, Domain, AdField,PlayerSettings } = require('../../models'); // Import Sequelize models
const path = require('path');
const fs = require('fs').promises;
const fsSync = require('fs'); // Synchronous fs module
const apicache = require('apicache');

const cache = apicache.middleware;

// มิดเดิลแวร์แคชแบบกำหนดเองเพื่อรวมเวลาแคชที่เหลือใน Headers
const customCache = (duration) => {
  return (req, res, next) => {
    const cachingMiddleware = cache(duration);
    cachingMiddleware(req, res, (err) => {
      if (!err) {
        const cacheEntry = apicache.getIndex().all[req.originalUrl];
        if (cacheEntry) {
          const remainingTime = Math.max(0, (cacheEntry.duration - (Date.now() - cacheEntry.timestamp)) / 1000);
          res.setHeader('X-Cache-Remaining', `${remainingTime.toFixed(0)} seconds`);
        }
      }
      next(err);
    });
  };
};


// Helper function to ensure directory existence
const ensureDirectoryExistence = async (filePath) => {
  const dirname = path.dirname(filePath);
  try {
    await fs.mkdir(dirname, { recursive: true });
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err;
    }
  }
};

// Helper function to check if cache file is expired
const isCacheExpired = async (filePath, ttlInSeconds) => {
  try {
    const stats = await fs.stat(filePath);
    const now = new Date().getTime();
    const fileTime = new Date(stats.mtime).getTime();
    return (now - fileTime) > ttlInSeconds * 1000;
  } catch (err) {
    // If file doesn't exist or another error occurs, consider cache as expired
    return true;
  }
};






router.get(['/hlsftp/:id/master', '/hlsftp/:id/master.m3u8'], async (req, res) => {
  const { id } = req.params;

  try {
    const video = await Video.findOne({ where: { md5: id } });
    if (!video) {
      return res.status(404).send('Video not found');
    }

    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).send('Server not found');
    }

    let qualities;
    try {
      qualities = JSON.parse(video.strQualities);
    } catch (parseError) {
      return res.status(500).json({ message: 'Error processing video qualities.' });
    }

    const dirPath = path.join(__dirname, `../../files/${video.md5}`);
    const filePath = path.join(dirPath, 'playlist.txt');
    const cacheFilePath = path.join(dirPath, 'playlist.cache');

    const cacheTTL = 3600; // 1 hour in seconds

    if (!(await isCacheExpired(cacheFilePath, cacheTTL))) {
      // Cache file exists and is not expired
      const cacheContent = await fs.readFile(cacheFilePath, 'utf8');
      res.type('text/plain');
      return res.send(cacheContent);
    }

    if (await fs.access(filePath).then(() => true).catch(() => false)) {
      let playlistContent = await fs.readFile(filePath, 'utf8');

      // Update audio URLs
      for (let i = 0; i <= 10; i++) {
        const regexPattern = new RegExp(`audio/audio_${i}\\.m3u8`, 'g');
        playlistContent = playlistContent.replace(regexPattern, () => {
          return `${process.env.BaseUrl}/filesftp/${video.md5}/audio/audio_${i}`;
        });
      }

      qualities.forEach((resolution) => {
        const regexPattern = new RegExp(`${resolution}/index\\.m3u8`, 'g');
        playlistContent = playlistContent.replace(regexPattern, () => {
          return `${process.env.BaseUrl}/filesftp/${video.md5}/${resolution}/index`;
        });
      });

      // Cache the playlist content
      await fs.writeFile(cacheFilePath, playlistContent);
      res.type('text/plain');
      return res.send(playlistContent);
    }
    const serverlist = JSON.parse(server.domains);
    const ipAddress = serverlist[0]; 
   //console.log(serverlist[0]);
    const playlistUrl = `https://${ipAddress}/files/${video.md5}/playerlist.m3u8`;

    const response = await axios.get(playlistUrl);

    await ensureDirectoryExistence(filePath);

    let updatedPlaylist = response.data;

    // Function to count audio segments
    function countAudioSegments(playlistContent) {
      const audioSegmentPattern = /#EXT-X-MEDIA:TYPE=AUDIO/g;
      const segments = playlistContent.match(audioSegmentPattern);
      const segmentCount = segments ? segments.length : 0;
      return segmentCount - 1;
    }
    const audioSegmentCount = countAudioSegments(updatedPlaylist);

    // Update audio URLs
    for (let i = 0; i <= audioSegmentCount; i++) {
      const audioFilePath = path.join(dirPath, 'audio', `audio_${i}.txt`);
      const audioURL = `https://${ipAddress}/files/${video.md5}/audio/audio_${i}.m3u8`;
      try {
        const res = await axios.get(audioURL);
        await ensureDirectoryExistence(audioFilePath);
        await fs.writeFile(audioFilePath, res.data);
      } catch (error) {
        
        return res.status(500).json({ message: 'Wait for the video to be processed.' },error);
      }

      const regexPattern = new RegExp(`audio/audio_${i}\\.m3u8`, 'g');
      updatedPlaylist = updatedPlaylist.replace(regexPattern, () => {
        return `${process.env.BaseUrl}/filesftp/${video.md5}/audio/audio_${i}`;
      });
    }

    await Promise.all(qualities.map(async (resolution) => {
      const resolutionWithoutP = resolution;
      const regexPattern = new RegExp(`${resolutionWithoutP}/index.m3u8`, 'g');
      updatedPlaylist = updatedPlaylist.replace(regexPattern, (match) => {
        return `${process.env.BaseUrl}/filesftp/${video.md5}/${resolutionWithoutP}/index`;
      });

      const resolutionURL = `https://${ipAddress}/files/${video.md5}/${resolutionWithoutP}/index.m3u8`;
      try {
        const res = await axios.get(resolutionURL);
        const resolutionFilePath = path.join(dirPath, resolutionWithoutP, 'index.txt');
        await ensureDirectoryExistence(resolutionFilePath);
        await fs.writeFile(resolutionFilePath, res.data);
      } catch (error) {
        return res.status(500).json({ message: 'Wait for the video to be processed.' });
      }
    }));

    await fs.writeFile(filePath, updatedPlaylist);
    res.type('text/plain');
    res.send(updatedPlaylist);

  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Wait for the video to be processed. ALL' });
  }
});

router.get('/filesftp/:id/:resolution/index',customCache('15 minutes'), async (req, res) => {
  try {
    const { id, resolution } = req.params;
    const cacheDir = path.join(__dirname, `../../cache/${id}/${resolution}`);
    const cacheFilePath = path.join(cacheDir, 'index.txt');

    // Create the cache directory if it doesn't exist
    if (!fsSync.existsSync(cacheDir)) {
      await fs.mkdir(cacheDir, { recursive: true });
    }

    if (fsSync.existsSync(cacheFilePath)) {
      // If the cached TXT file exists, read and send it
      const cachedPlaylist = await fs.readFile(cacheFilePath, 'utf8');
      res.type('text/plain').send(cachedPlaylist);
    } else {
      const video = await Video.findOne({ where: { md5: id } });
      if (!video) {
        return res.status(404).send('Video not found');
      }

      const server = await Server.findOne({ where: { id: video.strServer } });
      if (!server) {
        return res.status(404).send('Server not found');
      }

      let serverList;
      try {
        serverList = JSON.parse(server.domains);
      } catch (parseError) {
        return res.status(500).json({ message: 'Error processing video qualities.' });
      }

      const dirPath = path.join(__dirname, `../../files/${id}/`);
      const filePath = `${dirPath}${resolution}/index.txt`;

      if (fsSync.existsSync(filePath)) {
        let fileContent = await fs.readFile(filePath, 'utf8');
        let lines = fileContent.split('\n');

        // Remove the unwanted string from each line if present
        lines = lines.map(line => {
          if (line.includes(',IV=0x00000000000000000000000000000000')) {
            return line.replace(',IV=0x00000000000000000000000000000000', '');
          }
          return line;
        });
        lines = lines.map(line => {
          if (line.includes('sign.bin')) {
            return line.replace('sign.bin', `//hlsnew.movie-117.online/sign.bin`);
          }
          return line;
        });

        // Rest of your existing code for processing lines
        for (let i = 0; i < lines.length; i++) {
          if (lines[i].startsWith('#EXTINF:')) {
            const tsFile = lines[i + 1];
            if (tsFile) {
              lines[i + 1] = `${tsFile}`;
            }
          }
        }

        let i = 0;
        lines.forEach((line, index) => {
          if (line.includes('.html') || line.includes('.ts') || line.includes('.aaa') || line.includes('.jpg') || line.includes('.png')) {
            const modifiedLine = `https://${serverList[i]}/files/${id}/${resolution}/${line.replace('.jpg', '.jpg')}`;
            lines[index] = modifiedLine;
            i++;
            if (i >= serverList.length) {
              i = 0;
            }
          }
        });

        const updatedPlaylist = lines.join('\n');

        // Cache the updated playlist as a TXT file
        await fs.writeFile(cacheFilePath, updatedPlaylist, 'utf8');

        res.type('text/plain').send(updatedPlaylist);
      } else {
        res.status(404).json({ message: 'Playlist file not found' });
      }
    }
  } catch (error) {
    res.status(500).json({ message: 'Internal server error' });
  }
});

router.get('/filesftp/:id/audio/:audio',customCache('15 minutes'), async (req, res) => {
  try {
    const { id, audio } = req.params;
    const cacheDir = path.join(__dirname, `../../cache/${id}/audio/`);
    const cacheFilePath = path.join(cacheDir, `${audio}.txt`);

    // Create the cache directory if it doesn't exist
    if (!fsSync.existsSync(cacheDir)) {
      await fs.mkdir(cacheDir, { recursive: true });
    }

    if (fsSync.existsSync(cacheFilePath)) {
      // If the cached TXT file exists, read and send it
      const cachedPlaylist = await fs.readFile(cacheFilePath, 'utf8');
      res.type('text/plain').send(cachedPlaylist);
    } else {
      const video = await Video.findOne({ where: { md5: id } });
      if (!video) {
        return res.status(404).send('Video not found');
      }

      const server = await Server.findOne({ where: { id: video.strServer } });
      if (!server) {
        return res.status(404).send('Server not found');
      }

      let serverList;
      try {
        serverList = JSON.parse(server.domains);
      } catch (parseError) {
        return res.status(500).json({ message: 'Error processing server domains.' });
      }

      const dirPath = path.join(__dirname, `../../files/${id}/`);
      const filePath = `${dirPath}audio/${audio}.txt`;

      if (fsSync.existsSync(filePath)) {
        let fileContent = await fs.readFile(filePath, 'utf8');
        let lines = fileContent.split('\n');

        // Remove the unwanted string from each line if present
        lines = lines.map(line => {
          if (line.includes(',IV=0x00000000000000000000000000000000')) {
            return line.replace(',IV=0x00000000000000000000000000000000', '');
          }
          return line;
        });

        lines = lines.map(line => {
          if (line.includes('sign.bin')) {
            return line.replace('sign.bin', '//hlsnew.movie-117.online/sign.bin');
          }
          return line;
        });

        // Rest of your existing code for processing lines
        for (let i = 0; i < lines.length; i++) {
          if (lines[i].startsWith('#EXTINF:')) {
            const tsFile = lines[i + 1];
            if (tsFile) {
              lines[i + 1] = `${tsFile}`;
            }
          }
        }

        let i = 0;
        lines.forEach((line, index) => {
          if (line.includes('.html') || line.includes('.ts') || line.includes('.aaa') || line.includes('.jpg') || line.includes('.png')) {
            const modifiedLine = `https://${serverList[i]}/files/${id}/audio/${line.replace('.jpg', '.jpg')}`;
            lines[index] = modifiedLine;
            i++;
            if (i >= serverList.length) {
              i = 0;
            }
          }
        });

        const updatedPlaylist = lines.join('\n');

        // Cache the updated playlist as a TXT file
        await fs.writeFile(cacheFilePath, updatedPlaylist, 'utf8');

        res.type('text/plain').send(updatedPlaylist);
      } else {
        res.status(404).json({ message: 'Playlist file not found' });
      }
    }
  } catch (error) {
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;
