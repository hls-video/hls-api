require('dotenv').config();
const express = require('express');
const router = express.Router();
const axios = require('axios');
const { Video, Server,PlayerSettings } = require('../../models'); // Import Sequelize models
const path = require('path');
const fs = require('fs').promises;
const fsSync = require('fs'); // Synchronous fs module
const apicache = require('apicache');

let cache = apicache.middleware;

// Middleware to block IDM
const blockIDM = (req, res, next) => {
  const userAgent = req.headers['user-agent'];
  if (/IDM|Internet Download Manager/i.test(userAgent)) {
    return res.status(403).send('Access Denied');
  }
  next();
};

// มิดเดิลแวร์แคชแบบกำหนดเองเพื่อรวมเวลาแคชที่เหลือใน Headers
const customCache = (duration) => {
  return cache(duration, (req, res) => {
    return req.headers['x-skip-cache'] !== 'true';
  });
};
router.get('/clear-cache', (req, res) => {
  apicache.clear();
  res.send('Cache cleared');
});

// เส้นทางเล่นวิดีโอ
router.get('/playr2/:id', async (req, res) => {
  const { id } = req.params;
  try {
    const video = await Video.findOne({ where: { md5: id } });

    if (!video) {
      return res.status(404).send('ไม่พบวิดีโอ');
    }

    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).send('ไม่พบเซิร์ฟเวอร์');
    }

    const serverthumbnail = JSON.parse(server.domains);

    const m3u8 = `${process.env.BaseUrl}/hlsr2/${video.md5}/master.m3u8`;
    const m3u8txt = `${process.env.BaseUrl}/hlsr2/${video.md5}/master`;
    const thumbnail = `${process.env.BaseUrlR2}/files%2F${video.md5}%2Fthumbnail.jpg`;

    res.render('play/index', {
      videoTitle: video.strTitle,
      videomd5: video.md5,
      videostrQualities: video.strQualities,
      videoServer: video.strServer,
      m3u8,
      m3u8txt,
      thumbnail,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send('เกิดข้อผิดพลาดในการดึงข้อมูลวิดีโอ');
  }
});

// Helper function to ensure directory existence
const ensureDirectoryExistence = async (filePath) => {
  const dirname = path.dirname(filePath);
  try {
    await fs.mkdir(dirname, { recursive: true });
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err;
    }
  }
};

// Helper function to check if cache file is expired
const isCacheExpired = async (filePath, ttlInSeconds) => {
  try {
    const stats = await fs.stat(filePath);
    const now = new Date().getTime();
    const fileTime = new Date(stats.mtime).getTime();
    return (now - fileTime) > ttlInSeconds * 1000;
  } catch (err) {
    // If file doesn't exist or another error occurs, consider cache as expired
    return true;
  }
};

router.get(['/hlsr2/:id/master', '/hlsr2/:id/master.m3u8'], blockIDM, async (req, res) => {
  const { id } = req.params;

  try {

    const playerSettings = await PlayerSettings.findOne();
    if (!playerSettings) {
      return res.status(500).send('Player settings not found');
    }


    const referer = req.get('Referer');
    let refererDomain = '';

    if (referer) {
      try {
        const url = new URL(referer);
        refererDomain = url.hostname;
      } catch (err) {
       // console.error('Invalid Referer URL:', referer, err);
      }
    }

    // Extract domain from process.env.BaseUrl
    let baseDomain = '';
    try {
      const baseUrl = new URL(playerSettings.streamLinkDomains);
      baseDomain = baseUrl.hostname;
    } catch (err) {
      console.error('Invalid Base URL:', process.env.BaseUrl, err);
    }

    if (playerSettings.allowedPlaylist === false) {

      if (refererDomain !== baseDomain) {
        res.set('X-Skip-Cache', 'true');
        return res.status(404).send();
      }
    }
  

    const video = await Video.findOne({ where: { md5: id } });
    if (!video) {
      return res.status(404).send('Video not found');
    }

    const server = await Server.findOne({ where: { id: video.strServer } });
    if (!server) {
      return res.status(404).send('Server not found');
    }

    let qualities;
    try {
      qualities = JSON.parse(video.strQualities);
    } catch (parseError) {
      return res.status(500).json({ message: 'Error processing video qualities.' });
    }

    const dirPath = path.join(__dirname, `../../files/${video.md5}`);
    const filePath = path.join(dirPath, 'playlist.txt');
    const cacheFilePath = path.join(dirPath, 'playlist.cache');

    const cacheTTL = 3600; // 1 hour in seconds

    if (!(await isCacheExpired(cacheFilePath, cacheTTL))) {
      // Cache file exists and is not expired
      const cacheContent = await fs.readFile(cacheFilePath, 'utf8');
      res.type('text/plain');
      return res.send(cacheContent);
    }

    if (await fs.access(filePath).then(() => true).catch(() => false)) {
      let playlistContent = await fs.readFile(filePath, 'utf8');

      // Update audio URLs
      for (let i = 0; i <= 10; i++) {
        const regexPattern = new RegExp(`audio/audio_${i}\\.m3u8`, 'g');
        playlistContent = playlistContent.replace(regexPattern, () => {
          return `${process.env.BaseUrl}/filesr2/${video.md5}/audio/audio_${i}`;
        });
      }

      qualities.forEach((resolution) => {
        const regexPattern = new RegExp(`${resolution}/index\\.m3u8`, 'g');
        playlistContent = playlistContent.replace(regexPattern, () => {
          return `${process.env.BaseUrl}/filesr2/${video.md5}/${resolution}/index`;
        });
      });

      // Cache the playlist content
      await fs.writeFile(cacheFilePath, playlistContent);
      res.type('text/plain');
      return res.send(playlistContent);
    }

    const ipAddress = server.ip;
    const playlistUrl = `${process.env.BaseUrlR2}/files%2F${video.md5}%2Fplayerlist.m3u8`;

    const response = await axios.get(playlistUrl);

    await ensureDirectoryExistence(filePath);

    let updatedPlaylist = response.data;

    // Function to count audio segments
    function countAudioSegments(playlistContent) {
      const audioSegmentPattern = /#EXT-X-MEDIA:TYPE=AUDIO/g;
      const segments = playlistContent.match(audioSegmentPattern);
      const segmentCount = segments ? segments.length : 0;
      return segmentCount - 1;
    }
    const audioSegmentCount = countAudioSegments(updatedPlaylist);

    // Update audio URLs
    for (let i = 0; i <= audioSegmentCount; i++) {
      const audioFilePath = path.join(dirPath, 'audio', `audio_${i}.txt`);
      const audioURL = `${process.env.BaseUrlR2}/files%2F${video.md5}%2Faudio%2Faudio_${i}.m3u8`;
      try {
        const res = await axios.get(audioURL);
        await ensureDirectoryExistence(audioFilePath);
        await fs.writeFile(audioFilePath, res.data);
      } catch (error) {
        return res.status(500).json({ message: 'Wait for the video to be processed.' });
      }

      const regexPattern = new RegExp(`audio/audio_${i}\\.m3u8`, 'g');
      updatedPlaylist = updatedPlaylist.replace(regexPattern, () => {
        return `${process.env.BaseUrl}/filesr2/${video.md5}/audio/audio_${i}`;
      });
    }

    await Promise.all(qualities.map(async (resolution) => {
      const resolutionWithoutP = resolution;
      const regexPattern = new RegExp(`${resolutionWithoutP}/index.m3u8`, 'g');
      updatedPlaylist = updatedPlaylist.replace(regexPattern, (match) => {
        return `${process.env.BaseUrl}/filesr2/${video.md5}/${resolutionWithoutP}/index`;
      });

      const resolutionURL = `${process.env.BaseUrlR2}/files%2F${video.md5}%2F${resolutionWithoutP}%2Findex.m3u8`;
      try {
        const res = await axios.get(resolutionURL);
        const resolutionFilePath = path.join(dirPath, resolutionWithoutP, 'index.txt');
        await ensureDirectoryExistence(resolutionFilePath);
        await fs.writeFile(resolutionFilePath, res.data);
      } catch (error) {
        return res.status(500).json({ message: 'Wait for the video to be processed.' });
      }
    }));

    await fs.writeFile(filePath, updatedPlaylist);
    res.type('text/plain');
    res.send(updatedPlaylist);

  } catch (error) {
    res.status(500).json({ message: 'Wait for the video to be processed.' });
  }
});

router.get('/filesr2/:id/:resolution/index', blockIDM, async (req, res) => {
  try {
    const { id, resolution } = req.params;


    const playerSettings = await PlayerSettings.findOne();
    if (!playerSettings) {
      return res.status(500).send('Player settings not found');
    }


    const referer = req.get('Referer');
    let refererDomain = '';

    if (referer) {
      try {
        const url = new URL(referer);
        refererDomain = url.hostname;
      } catch (err) {
        console.error('Invalid Referer URL:', referer, err);
      }
    }

      // Extract domain from process.env.BaseUrl
      let baseDomain = '';
      try {
        const baseUrl = new URL(playerSettings.streamLinkDomains);
        baseDomain = baseUrl.hostname;
      } catch (err) {
        console.error('Invalid Base URL:', playerSettings.streamLinkDomains, err);
      }
  
      if (playerSettings.allowedPlaylistIndex === false) {
  
        if (refererDomain !== baseDomain) {
          res.set('X-Skip-Cache', 'true');
          return res.status(404).send();
        }
      }
    

    const cacheDir = path.join(__dirname, `../../cache/${id}/${resolution}`);
    const cacheFilePath = path.join(cacheDir, 'index.txt');

    // Create the cache directory if it doesn't exist
    if (!fsSync.existsSync(cacheDir)) {
      await fs.mkdir(cacheDir, { recursive: true });
    }

    if (fsSync.existsSync(cacheFilePath)) {
      // If the cached TXT file exists, read and send it
      const cachedPlaylist = await fs.readFile(cacheFilePath, 'utf8');
      res.type('text/plain').send(cachedPlaylist);
    } else {
      const video = await Video.findOne({ where: { md5: id } });
      if (!video) {
        return res.status(404).send('Video not found');
      }

      const server = await Server.findOne({ where: { id: video.strServer } });
      if (!server) {
        return res.status(404).send('Server not found');
      }

      let serverList;
      try {
        serverList = JSON.parse(server.domains);
      } catch (parseError) {
        return res.status(500).json({ message: 'Error processing video qualities.' });
      }

      const dirPath = path.join(__dirname, `../../files/${id}/`);
      const filePath = `${dirPath}${resolution}/index.txt`;

      if (fsSync.existsSync(filePath)) {
        let fileContent = await fs.readFile(filePath, 'utf8');
        let lines = fileContent.split('\n');

        // Remove the unwanted string from each line if present
        lines = lines.map(line => {
          if (line.includes(',IV=0x00000000000000000000000000000000')) {
            return line.replace(',IV=0x00000000000000000000000000000000', '');
          }
          return line;
        });
        lines = lines.map(line => {
          if (line.includes('sign.bin')) {
            return line.replace('sign.bin', `//hlsnew.movie-117.online/sign.bin`);
          }
          return line;
        });

        // Rest of your existing code for processing lines
        for (let i = 0; i < lines.length; i++) {
          if (lines[i].startsWith('#EXTINF:')) {
            const tsFile = lines[i + 1];
            if (tsFile) {
              lines[i + 1] = `${tsFile}`;
            }
          }
        }

        let i = 0;
        lines.forEach((line, index) => {
          if (line.includes('.html') || line.includes('.ts') || line.includes('.aaa') || line.includes('.jpg') || line.includes('.png')) {
            const modifiedLine = `//${serverList[i]}/files/${id}%2F${resolution}%2F${line.replace('.jpg', '.jpg')}`;
            lines[index] = modifiedLine;
            i++;
            if (i >= serverList.length) {
              i = 0;
            }
          }
        });

        const updatedPlaylist = lines.join('\n');

        // Cache the updated playlist as a TXT file
        await fs.writeFile(cacheFilePath, updatedPlaylist, 'utf8');

        res.type('text/plain').send(updatedPlaylist);
      } else {
        res.status(404).json({ message: 'Playlist file not found' });
      }
    }
  } catch (error) {
    res.status(500).json({ message: 'Internal server error' });
  }
});

router.get('/filesr2/:id/audio/:audio', blockIDM, async (req, res) => {
  try {
    const { id, audio } = req.params;


    const playerSettings = await PlayerSettings.findOne();
    if (!playerSettings) {
      return res.status(500).send('Player settings not found');
    }


    const referer = req.get('Referer');
    let refererDomain = '';

    if (referer) {
      try {
        const url = new URL(referer);
        refererDomain = url.hostname;
      } catch (err) {
        //console.error('Invalid Referer URL:', referer, err);
      }
    }

    // Extract domain from process.env.BaseUrl
    let baseDomain = '';
    try {
      const baseUrl = new URL(playerSettings.streamLinkDomains);
      baseDomain = baseUrl.hostname;
    } catch (err) {
      console.error('Invalid Base URL:', playerSettings.streamLinkDomains, err);
    }
    if (playerSettings.allowedPlaylistIndex === false) {

      if (refererDomain !== baseDomain) {
        res.set('X-Skip-Cache', 'true');
        return res.status(404).send();
      }
    }
  
    const cacheDir = path.join(__dirname, `../../cache/${id}/audio/`);
    const cacheFilePath = path.join(cacheDir, `${audio}.txt`);

    // Create the cache directory if it doesn't exist
    if (!fsSync.existsSync(cacheDir)) {
      await fs.mkdir(cacheDir, { recursive: true });
    }

    if (fsSync.existsSync(cacheFilePath)) {
      // If the cached TXT file exists, read and send it
      const cachedPlaylist = await fs.readFile(cacheFilePath, 'utf8');
      res.type('text/plain').send(cachedPlaylist);
    } else {
      const video = await Video.findOne({ where: { md5: id } });
      if (!video) {
        return res.status(404).send('Video not found');
      }

      const server = await Server.findOne({ where: { id: video.strServer } });
      if (!server) {
        return res.status(404).send('Server not found');
      }

      let serverList;
      try {
        serverList = JSON.parse(server.domains);
      } catch (parseError) {
        return res.status(500).json({ message: 'Error processing server domains.' });
      }

      const dirPath = path.join(__dirname, `../../files/${id}/`);
      const filePath = `${dirPath}audio/${audio}.txt`;

      if (fsSync.existsSync(filePath)) {
        let fileContent = await fs.readFile(filePath, 'utf8');
        let lines = fileContent.split('\n');

        // Remove the unwanted string from each line if present
        lines = lines.map(line => {
          if (line.includes(',IV=0x00000000000000000000000000000000')) {
            return line.replace(',IV=0x00000000000000000000000000000000', '');
          }
          return line;
        });

        lines = lines.map(line => {
          if (line.includes('sign.bin')) {
            return line.replace('sign.bin', '//hlsnew.movie-117.online/sign.bin');
          }
          return line;
        });

        // Rest of your existing code for processing lines
        for (let i = 0; i < lines.length; i++) {
          if (lines[i].startsWith('#EXTINF:')) {
            const tsFile = lines[i + 1];
            if (tsFile) {
              lines[i + 1] = `${tsFile}`;
            }
          }
        }

        let i = 0;
        lines.forEach((line, index) => {
          if (line.includes('.html') || line.includes('.ts') || line.includes('.aaa') || line.includes('.jpg') || line.includes('.png')) {
            const modifiedLine = `//${serverList[i]}/files%2F${id}%2Faudio%2F${line.replace('.jpg', '.jpg')}`;
            lines[index] = modifiedLine;
            i++;
            if (i >= serverList.length) {
              i = 0;
            }
          }
        });

        const updatedPlaylist = lines.join('\n');

        // Cache the updated playlist as a TXT file
        await fs.writeFile(cacheFilePath, updatedPlaylist, 'utf8');

        res.type('text/plain').send(updatedPlaylist);
      } else {
        res.status(404).json({ message: 'Playlist file not found' });
      }
    }
  } catch (error) {
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;
