require("dotenv").config();
const express = require("express");
const router = express.Router();
const { OAuth2Client } = require("google-auth-library");
const { GoogleAccount, sequelize } = require("../../models");

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  let userIdCookie = req.cookies.id;
  if (tokenCookie) {
    req.session.user = {
      id: userIdCookie,
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// Route for displaying the Google account settings page
router.get("/google-drive", isAuthenticated, async (req, res) => {
  try {
    const googleAccounts = await GoogleAccount.findAll();
    // res.render('google/google_account', {
    //   google: googleAccounts,
    //   username: req.session.user.username,
    //   userid: req.session.user.id,
    //   apiKey: req.session.user.apiKey,
    // });
    return res.status(200).json({
      google: googleAccounts,
      username: req.session.user.username,
      userid: req.session.user.id,
      apiKey: req.session.user.apiKey,
    });
  } catch (err) {
    res.status(500).send("Error fetching Google account data");
  }
});

router.get("/google-add", isAuthenticated, async (req, res) => {
  try {
    res.render("google/google_add", {
      username: req.session.user.username,
      userid: req.session.user.id,
      apiKey: req.session.user.apiKey,
    });
  } catch (err) {
    res.status(500).send("Error fetching Google account data");
  }
});

// Route for adding a Google account
router.post("/google-add", isAuthenticated, async (req, res) => {
  try {
    const { email, clientId, clientsecret, refreshtoken } = req.body;

    // Check for duplicate email
    const existingAccount = await GoogleAccount.findOne({ where: { email } });

    if (existingAccount) {
      return res
        .status(400)
        .send("This email is already associated with a Google Drive account");
    }

    await GoogleAccount.create({
      email: email,
      clientId: clientId,
      clientsecret: clientsecret, // Ensure this matches exactly
      refreshtoken: refreshtoken, // Ensure this matches exactly
      status: "1",
      created_at: sequelize.fn("NOW"),
    });

    res.status(200).send("Google Drive account added successfully");
  } catch (err) {
    console.error("Error adding Google Drive account:", err); // Improved error logging
    res.status(500).send("Error handling request");
  }
});

module.exports = router;
