require("dotenv").config();
const express = require("express");
const router = express.Router();
const { Server, GoogleSettings, PlayerSettings } = require("../../models"); // Import Sequelize models

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  let userIdCookie = req.cookies.id;
  if (tokenCookie) {
    req.session.user = {
      id: userIdCookie,
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// Server List
router.get("/setting-google-drive", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    const userSettings = await GoogleSettings.findOne({
      where: {
        userId: req.session.user.id,
      },
    });

    // res.render('google/setting_google_drive', {
    //   servers,
    //   username: req.session.user.username,
    //   apiKey: req.session.user.apiKey,
    //   userid: req.session.user.id,
    //   userSettings
    // });
    return res.status(200).json({
      servers,
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
      userid: req.session.user.id,
      userSettings,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Error fetching server data");
  }
});

router.post("/save-settings", isAuthenticated, async (req, res) => {
  try {
    const data = req.body;
    const existingSettings = await GoogleSettings.findOne({
      where: {
        userId: req.session.user.id,
      },
    });

    if (existingSettings) {
      // Update existing settings
      Object.assign(existingSettings, {
        ip: data.ip,
        port: data.port,
        username: data.username,
        password: data.password,
        reprocess360: data.reprocess360 ? 1 : 0,
        reprocess720: data.reprocess720 ? 1 : 0,
        reprocess1080: data.reprocess1080 ? 1 : 0,
        origin: data.origin ? 1 : 0,
        proxy360: data.proxy360 ? 1 : 0,
        proxy720: data.proxy720 ? 1 : 0,
        proxy1080: data.proxy1080 ? 1 : 0,
        proxyhigh1080_360: data.proxyhigh1080_360 ? 1 : 0,
        proxyhigh720: data.proxyhigh720 ? 1 : 0,
        proxyhigh1080: data.proxyhigh1080 ? 1 : 0,
        noproxy: data.noproxy ? 1 : 0,
      });
      await existingSettings.save();
      res.send({
        message: "Settings updated successfully",
        settings: existingSettings,
      });
    } else {
      // Create new settings
      const newSettings = await GoogleSettings.create({
        userId: req.session.user.id,
        ip: data.ip,
        port: data.port,
        username: data.username,
        password: data.password,
        reprocess360: data.reprocess360 ? 1 : 0,
        reprocess720: data.reprocess720 ? 1 : 0,
        reprocess1080: data.reprocess1080 ? 1 : 0,
        origin: data.origin ? 1 : 0,
        proxy360: data.proxy360 ? 1 : 0,
        proxy720: data.proxy720 ? 1 : 0,
        proxy1080: data.proxy1080 ? 1 : 0,
        proxyhigh1080_360: data.proxyhigh1080_360 ? 1 : 0,
        proxyhigh720: data.proxyhigh720 ? 1 : 0,
        noproxy: data.noproxy ? 1 : 0,
      });
      res.send({
        message: "Settings created successfully",
        settings: newSettings,
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Database error", details: error.message });
  }
});

router.get("/setting-player", isAuthenticated, async (req, res) => {
  try {
    const userId = req.session.user.id; // Assuming you have session management in place

    const playerSettings = await PlayerSettings.findOne({
      where: { userId: userId },
    });

    // res.render("setting/setting_player", {
    //   settings: playerSettings || {},
    //   username: req.session.user.username,
    //   apiKey: req.session.user.apiKey,
    //   userid: req.session.user.id,
    // });
    return res.status(200).json({
      settings: playerSettings || {},
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
      userid: req.session.user.id,
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Database error", details: error.message });
  }
});

router.post("/setting-player", isAuthenticated, async (req, res) => {
  try {
    const data = req.body;
    const userId = req.session.user.id; // Assuming you have session management in place

    const [playerSettings, created] = await PlayerSettings.findOrCreate({
      where: { userId: userId },
      defaults: {
        playerUrl: data.playerUrl,
        iframeHeight: data.iframeHeight,
        iframeWidth: data.iframeWidth,
        jwplayerKey: data.jwplayerKey,
        jwplayerUrl: data.jwplayerUrl,
        defaultImage: data.defaultImage,
        rememberPosition: data.rememberPosition,
        logoUrl: data.logoUrl,
        logoTarget: data.logoTarget,
        logoPosition: data.logoPosition,
        allowedSites: data.allowedSites,
        streamLinkDomains: data.streamLinkDomains,
        p2pLink: data.p2pLink,
        streamApiKey: data.streamApiKey,
        playerRememberPosition: data.playerRememberPosition ? 1 : 0,
        logoSwitchChack: data.logoSwitchChack ? 1 : 0,
        logoSwitchHide: data.logoSwitchHide ? 1 : 0,
        allowedPlaylist: data.allowedPlaylist ? 1 : 0,
        allowedPlaylistIndex: data.allowedPlaylistIndex ? 1 : 0,
        allowedP2P: data.allowedP2P ? 1 : 0,
        allowedApiNuxt: data.allowedApiNuxt ? 1 : 0,
      },
    });

    if (!created) {
      // Update existing settings
      Object.assign(playerSettings, {
        playerUrl: data.playerUrl,
        iframeHeight: data.iframeHeight,
        iframeWidth: data.iframeWidth,
        jwplayerKey: data.jwplayerKey,
        jwplayerUrl: data.jwplayerUrl,
        defaultImage: data.defaultImage,
        rememberPosition: data.rememberPosition,
        logoUrl: data.logoUrl,
        logoTarget: data.logoTarget,
        logoPosition: data.logoPosition,
        allowedSites: data.allowedSites,
        streamLinkDomains: data.streamLinkDomains,
        p2pLink: data.p2pLink,
        streamApiKey: data.streamApiKey,
        playerRememberPosition: data.playerRememberPosition ? 1 : 0,
        logoSwitchChack: data.logoSwitchChack ? 1 : 0,
        logoSwitchHide: data.logoSwitchHide ? 1 : 0,
        allowedPlaylist: data.allowedPlaylist ? 1 : 0,
        allowedPlaylistIndex: data.allowedPlaylistIndex ? 1 : 0,
        allowedP2P: data.allowedP2P ? 1 : 0,
        allowedApiNuxt: data.allowedApiNuxt ? 1 : 0,
      });

      await playerSettings.save();
    }

    res.send({
      message: created
        ? "Settings created successfully"
        : "Settings updated successfully",
      settings: playerSettings,
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Database error", details: error.message });
  }
});

module.exports = router;
