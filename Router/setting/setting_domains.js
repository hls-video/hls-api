//setting_domains.js
require("dotenv").config();
const express = require("express");
const router = express.Router();
const {
  Server,
  GoogleSettings,
  PlayerSettings,
  Domain,
  AdField,
} = require("../../models"); // Import Sequelize models

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  let usernameCookie = req.cookies.username;
  let userIdCookie = req.cookies.id;
  if (tokenCookie) {
    req.session.user = {
      id: userIdCookie,
      username: usernameCookie,
      apiKey: tokenCookie,
    };
    // req.session.user.apiKey = tokenCookie;
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401).send("Unauthorized");
  }
}

// Get domains and render page
router.get("/setting-domains", isAuthenticated, async (req, res) => {
  try {
    const servers = await Server.findAll();
    const userSettings = await GoogleSettings.findOne({
      where: {
        userId: req.session.user.id,
      },
    });
    const domains = await Domain.findAll({
      include: [{ model: AdField, as: "adFields" }],
    });

    // Format domains and adFields for easier use in the template
    const formattedDomains = domains.map((domain) => ({
      id: domain.id,
      domainName: domain.domainName,
      defaultBgImage: domain.defaultBgImage,
      logoURL: domain.logoURL,
      logoLink: domain.logoLink,
      logoPosition: domain.logoPosition,
      adActive: domain.adActive,
      adFields: domain.adFields.map((adField) => ({
        adURL: adField.adURL,
        adLink: adField.adLink,
        adTime: adField.adTime,
      })), // Ensure adFields is an array of objects with required fields
    }));

    // res.render('setting/setting_domains', {
    //   servers,
    //   domains: formattedDomains,
    //   username: req.session.user.username,
    //   apiKey: req.session.user.apiKey,
    //   userid: req.session.user.id,
    //   userSettings
    // });
    return res.status(200).json({
      servers,
      domains: formattedDomains,
      username: req.session.user.username,
      apiKey: req.session.user.apiKey,
      userid: req.session.user.id,
      userSettings,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Error fetching data");
  }
});

// Add a new domain
router.post("/settings-domains-add", async (req, res) => {
  const {
    domainName,
    defaultBgImage,
    hideLogo,
    logoURL,
    logoLink,
    logoPosition,
    adActive,
    adFields,
    iMemberID,
  } = req.body;

  if (!iMemberID) {
    return res.status(400).json({ error: "iMemberID is required" });
  }

  try {
    const domain = await Domain.create({
      domainName,
      defaultBgImage,
      hideLogo,
      logoURL,
      logoLink,
      logoPosition,
      adActive,
      iMemberID,
    });

    if (adFields && adFields.length > 0) {
      for (const adField of adFields) {
        await AdField.create({
          adURL: adField.adURL,
          adLink: adField.adLink,
          adTime: adField.adTime,
          domainId: domain.id,
        });
      }
    }

    res.status(201).json(domain);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Update a domain
router.put("/settings-domains/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const domain = await Domain.findByPk(id);

    if (!domain) {
      return res.status(404).json({ error: "Domain not found" });
    }

    await domain.update(req.body);

    // Update ad fields
    if (req.body.adFields && req.body.adFields.length > 0) {
      await AdField.destroy({ where: { domainId: id } });
      for (const adField of req.body.adFields) {
        await AdField.create({
          adURL: adField.adURL,
          adLink: adField.adLink,
          adTime: adField.adTime,
          domainId: id,
        });
      }
    }

    res.json(domain);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

// Delete a domain
router.delete("/settings-domains/:id", async (req, res) => {
  try {
    const { id } = req.params;
    await AdField.destroy({ where: { domainId: id } });
    await Domain.destroy({ where: { id } });

    res.json({ message: "Domain deleted successfully" });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

module.exports = router;
