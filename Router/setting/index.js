// index.js
require('dotenv').config();
const express = require('express');
const router = express.Router();
const playerRouter = require('./setting_player');
const domainRouter = require('./setting_domains');

router.use('/', playerRouter);
router.use('/', domainRouter);

module.exports = router;