const express = require('express');
const router = express.Router();
const WebSocket = require('ws'); // Import WebSocket

module.exports = function(wss) {
  let storedLogData = []; // Array for storing log data

  router.post('/api/logs', (req, res) => {
    try {
      const logData = req.body;
      storedLogData.push(logData);
      //console.log('Received log:', logData);

      // Broadcast the new log to all connected WebSocket clients
      wss.clients.forEach(client => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify(logData));
        }
      });

      res.status(200).send('Log received');
    } catch (error) {
      console.error('Error receiving log:', error);
      res.status(500).send('Error receiving log');
    }
  });

  router.get('/server/logs', (req, res) => {
    try {
      res.json(storedLogData);
    } catch (error) {
      console.error('Error fetching logs:', error);
      res.status(500).send('Error fetching logs');
    }
  });

  return router;
};