const express = require('express');
const router = express.Router();
const { Video, GoogleAccount } = require('../../models'); // Import Sequelize models

// Middleware to check if user is authenticated
function isAuthenticated(req, res, next) {
  let tokenCookie = req.cookies.token;
  console.log("🚀 ~ isAuthenticated ~ tokenCookie:", tokenCookie)
  if (tokenCookie) {
    return next();
  } else {
    // res.redirect('/auth/login');
    return res.status(401);
  }
}

// Route to upload video data
router.post('/api/upload/videos', async (req, res) => {
  try {
    let { strTitle, TotalSize, strServer, iMemberID, strFileName, strQualities, timeadd, timeupdate, md5, iType, strUrl = '' } = req.body;

    // Truncate strTitle if it's too long
    if (strTitle.length > 80) {
      strTitle = strTitle.substring(0, 80);
    }

    await Video.create({
      strTitle,
      TotalSize,
      strServer,
      iMemberID,
      strFileName,
      strQualities,
      timeadd,
      timeupdate,
      md5,
      iType,
      strUrl
    });

    res.json({ success: true, message: 'Video uploaded successfully' });
  } catch (err) {
    console.error('Error uploading video:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
});

// Helper function to process videos based on status and type
const processVideos = async (req, res, status, type = null) => {
  try {
    const processCheck = await Video.count({ where: { iStatus: status } });

    if (processCheck > 0) {
      return res.status(200).json([]);
    }

    const whereClause = { iStatus: 0 };
    if (type) {
      whereClause.iType = type;
    }

    const videos = await Video.findAll({
      where: whereClause,
      order: [['id', 'ASC']],
      limit: 1
    });

    res.status(200).json(videos);
  } catch (error) {
    console.error('Error processing videos:', error);
    res.status(500).json({ message: 'Error retrieving videos' });
  }
};

router.get('/api/videos/process', (req, res) => processVideos(req, res, 3, 'upload'));
router.get('/api/videos/process2', (req, res) => processVideos(req, res, 2));
router.get('/api/videos/process3', (req, res) => processVideos(req, res, 3));
router.get('/api/videos/process-google-drive', (req, res) => processVideos(req, res, 3, 'google-drive'));
router.get('/api/videos/process-remotmp4', (req, res) => processVideos(req, res, 3, 'remote-mp4'));
router.get('/api/videos/process-remote-m3u8', (req, res) => processVideos(req, res, 3, 'remote-m3u8'));

// Route to update video status
router.post('/api/videos/update', async (req, res) => {
  const { videoId, status, resolutions, size, strTitle } = req.body;

  try {
    const updateFields = {
      iStatus: status,
      strQualities: JSON.stringify(resolutions),
      TotalSize: size
    };

    if (strTitle) {
      updateFields.strTitle = strTitle;
    }

    const [updated] = await Video.update(updateFields, {
      where: { id: videoId }
    });

    if (updated > 0) {
      res.status(200).send({ message: 'Video status updated successfully' });
    } else {
      res.status(404).send({ message: 'Video not found' });
    }
  } catch (error) {
    console.error('Error updating video status:', error);
    res.status(500).send({ message: 'Error updating video status' });
  }
});

// Route to get Google account details
router.get('/api/google_account', async (req, res) => {
  try {
    const accounts = await GoogleAccount.findAll({
      where: { status: 1 },
      order: [['id', 'ASC']],
      limit: 1
    });
    res.status(200).json(accounts);
  } catch (error) {
    console.error('Error retrieving Google accounts:', error);
    res.status(500).json({ message: 'Error retrieving Google accounts' });
  }
});

router.get('/api/v1/play/65343db00b25746d2a7c66d2', async (req, res) => {
  try {
    const movieDetails = {
      success: true,
      movie: {
        videos: [
          {
            url: "https://main.77player.xyz/?id=342f4a41bdabad975264f7b1",
            languages: "thai_dubbing"
          },
          {
            url: "https://main.77player.xyz/?id=079c0c363b854dc4e0e5818d",
            languages: "thai_subtitles"
          }
        ]
      }
    };
    res.status(200).json(movieDetails);
  } catch (error) {
    console.error('Error retrieving movie details:', error);
    res.status(500).json({ message: 'Error retrieving movie details' });
  }
});




module.exports = router;
