const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackObfuscator = require('webpack-obfuscator');

module.exports = {
  entry: './src/index.js', // Entry point for your application
  output: {
    filename: 'bundle.js', // Output bundle file name
    path: path.resolve(__dirname, 'dist'), // Output directory path
  },
  module: {
    rules: [
      {
        test: /\.css$/i, // Test for .css files
        use: ['style-loader', 'css-loader'], // Use style-loader and css-loader
      },
    ],
  },
  resolve: {
    fallback: {
      "util": require.resolve("util/"), // Fallback for util module
      "path": require.resolve("path-browserify"), // Fallback for path module
      "crypto": require.resolve("crypto-browserify"), // Fallback for crypto module
      "stream": require.resolve("stream-browserify"), // Fallback for stream module
      "assert": require.resolve("assert/"), // Fallback for assert module
      "http": require.resolve("stream-http"), // Fallback for http module
      "https": require.resolve("https-browserify"), // Fallback for https module
      "os": require.resolve("os-browserify/browser"), // Fallback for os module
      "buffer": require.resolve("buffer/"), // Fallback for buffer module
      "vm": require.resolve("vm-browserify"), // Fallback for vm module
      "zlib": require.resolve("browserify-zlib"), // Fallback for zlib module
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html', // Path to your HTML file
      inject: 'body',
      scriptLoading: 'blocking', // Ensure scripts are loaded in order
      cdn: {
        css: [
          'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
        ],
        js: [
          'https://code.jquery.com/jquery-3.6.0.min.js',
          'https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js',
          'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js',
          'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js'
        ]
      }
    }),
    new WebpackObfuscator ({
      compact: true,
      controlFlowFlattening: true,
      controlFlowFlatteningThreshold: 0.75,
      deadCodeInjection: true,
      deadCodeInjectionThreshold: 0.4,
      rotateStringArray: true,
      stringArray: true,
      stringArrayThreshold: 0.75
    }, ['excluded_bundle_name.js'])
  ],
};
