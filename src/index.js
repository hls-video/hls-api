import './css/player.css';


document.addEventListener('DOMContentLoaded', () => {
    const playlistDataElement = document.getElementById('playlist-data');
    if (!playlistDataElement) {
      console.error('Element with ID "playlist-data" not found.');
      return;
    }
  
    const playlistDataText = playlistDataElement.textContent;
    if (!playlistDataText) {
      console.error('Element "playlist-data" has no content.');
      return;
    }
  
    try {
      const playlist = JSON.parse(playlistDataText);
      initializePlayer(playlist);
    } catch (error) {
      console.error('Error parsing playlist data:', error);
    }
  });
  
  function initializePlayer(playlist) {
    let player = null;
    let myTimer = null;
    let e_beforeplay = 0;
    let playAds = "play";
  
    function setupPlayer() {
      if (playlist.length === 0) {
        playMovie(false); // Directly play the main video without autostart
        return;
      }
  
      player = jwplayer("video_container1").setup({
        playlist: playlist,
        mute: false,
        enableFullscreen: false,
        preload: "none",
        volume: 70,
        androidhls: true,
      });
  
      player.on("play", function () {
        if (e_beforeplay === 0) {
          e_beforeplay = 1;
          if ($(".moviePlayer .ad-player-skip").length === 0) {
            $("#video_container1").append(`
              <div class="moviePlayer ad-player-skip">
                <div class="ad-overlay-click"></div>
                <div class="ad-player-link">
                  <a href="" target="_blank" class="btn btn-ad-link">สมัครสมาชิก</a>
                </div>
                <button class="btn btn-skip" onclick="next(this)">
                  <span>กดข้ามได้ใน</span>
                </button>
              </div>
            `);
          }
          clock();
          $(".btn-ad-link").attr("href", playlist[player.getPlaylistIndex()].sources[0].clickUrl);
          setSkipTime(player.getPlaylistIndex());
        }
      });
  
      player.on("playlistComplete", function () {
        playMovie(true); // Play the main video with autostart after ads
      });
  
      player.on("error", function () {
        console.log("Error occurred");
        forceNext();
      });
    }
  
    function setSkipTime(index) {
      const skipTime = playlist[index].sources[0].skiptime;
      clock(skipTime);
    }
  
    function next(button) {
      if ($(button).find("span").html() !== "ข้ามโฆษณา") {
        return false;
      }
      $(".btn-skip span").html("กดข้ามได้ใน");
      forceNext();
    }
  
    function playVideo(index) {
      player.playlistItem(index);
    }
  
    function forceNext() {
      clearInterval(myTimer);
      e_beforeplay = 0;
      const currentIndex = player.getPlaylistIndex();
      if (currentIndex === playlist.length - 1) {
        playMovie(true); // Play the main video with autostart
      } else {
        player.next();
      }
    }
  
    function playMovie(autostart) {
      playAds = "donplay";
      const videoContainer = document.getElementById("video_container1");
      if (videoContainer) {
        const videoElements = videoContainer.getElementsByTagName("video");
        if (videoElements.length > 0) {
          videoElements[0].pause();
        }
        videoContainer.style.display = "none";
      }
  
      const objSetup = {
        preload: "metadata",
        primary: "html5",
        hlshtml: true,
        controls: true,
        pipIcon: true,
        autostart: autostart,
        mute: false,
        repeat: false,
        image: image,
        logo: {
          file: logo_file, // Replace with your logo URL
          link: logo_Link, // Replace with your logo link URL
          position: logo_logoPosition, // Position of the logo
          hide: true,
        },
        sources: [{
          file: hls,
          type: "application/vnd.apple.mpegurl"
        }],
      };
  
      jwplayer("playerm3u8").setup(objSetup);
  
      jwplayer("playerm3u8").on('ready', function (evt) {
        console.log("Player is ready");
        clientSide.removeBtn();
        clientSide.forwardBtn();
      });
  
      jwplayer("playerm3u8").on('levels', function (e) {
        levelQ = e?.levels;
      });
  
      jwplayer("playerm3u8").on('levelsChanged', function (e) {
        clientSide.qualitySwitch(e.currentQuality);
      });
  
      jwplayer("playerm3u8").on('visualQuality', function (e) {
        clientSide.qualitySwitch(e.level.index);
      });
    }
  
    function clock(time) {
      clearInterval(myTimer);
      let remainingTime = parseInt(time, 10);
  
      function updateTimer() {
        if (remainingTime <= 0) {
          clearInterval(myTimer);
          $(".btn-skip span").html("ข้ามโฆษณา");
        } else {
          $(".btn-skip span").html(`กดข้ามได้ใน (${remainingTime})`);
        }
        remainingTime--;
      }
  
      updateTimer();
      myTimer = setInterval(updateTimer, 1000);
    }
  
    function showSpinner() {
      document.getElementById("spinnerOverlay").style.display = "flex";
    }
  
    function hideSpinner() {
      document.getElementById("spinnerOverlay").style.display = "none";
    }
  
    setupPlayer();
  
    let levelQ, clientSide = {
      qualitySwitch: function (b) {
        let item = levelQ[b];
        if (this.svgLabel(item?.label) == undefined) {
          jwplayer("playerm3u8").removeButton("qSwitch");
        } else {
          jwplayer("playerm3u8").addButton(this.svgLabel(item?.label), item?.label, function () {
          }, "qSwitch");
        }
      },
      removeBtn: function () {
        jwplayer("playerm3u8").removeButton("share");
      },
      forwardBtn: function () {
        // display icon
        let iconForward = `<svg xmlns="http://www.w3.org/2000/svg" class="jw-svg-icon jw-svg-icon-rewind" viewBox="0 0 240 240" focusable="false"> <path d="M185,135.6c-3.7-6.3-10.4-10.3-17.7-10.6c-7.3,0.3-14,4.3-17.7,10.6c-8.6,14.2-8.6,32.1,0,46.3c3.7,6.3,10.4,10.3,17.7,10.6 c7.3-0.3,14-4.3,17.7-10.6C193.6,167.6,193.6,149.8,185,135.6z M167.3,182.8c-7.8,0-14.4-11-14.4-24.1s6.6-24.1,14.4-24.1 s14.4,11,14.4,24.1S175.2,182.8,167.3,182.8z M123.9,192.5v-51l-4.8,4.8l-6.8-6.8l13-13c1.9-1.9,4.9-1.9,6.8,0 c0.9,0.9,1.4,2.1,1.4,3.4v62.7L123.9,192.5z M22.7,57.4h130.1V38.1c0-5.3,3.6-7.2,8-4.3l41.8,27.9c1.2,0.6,2.1,1.5,2.7,2.7 c1.4,3,0.2,6.5-2.7,8l-41.8,27.9c-4.4,2.9-8,1-8-4.3V76.7H37.1v96.4h48.2v19.3H22.6c-2.6,0-4.8-2.2-4.8-4.8V62.3 C17.8,59.6,20,57.4,22.7,57.4z"> </path> </svg>`;
        const rewindContainer = document.querySelector('.jw-display-icon-rewind');
        const forwardContainer = rewindContainer.cloneNode(true);
        const forwardDisplayButton = forwardContainer.querySelector('.jw-icon-rewind');
        forwardDisplayButton.style.transform = "scaleX(-1)";
        forwardDisplayButton.ariaLabel = "Forward 10 Seconds";
        const nextContainer = document.querySelector('.jw-display-icon-next');
        nextContainer.parentNode.insertBefore(forwardContainer, nextContainer);
  
        // control bar icon
        document.querySelector('.jw-display-icon-next').style.display = 'none'; // hide next button
        const buttonContainer = document.querySelector('.jw-button-container');
        const rewindControlBarButton = buttonContainer.querySelector(".jw-icon-rewind");
        const forwardControlBarButton = rewindControlBarButton.cloneNode(true);
        forwardControlBarButton.style.transform = "scaleX(-1)";
        forwardControlBarButton.ariaLabel = "Forward 10 Seconds";
        rewindControlBarButton.parentNode.insertBefore(forwardControlBarButton, rewindControlBarButton.nextElementSibling);
        // add onclick handlers
        [forwardDisplayButton, forwardControlBarButton].forEach(button => {
          button.onclick = () => {
            jwplayer("playerm3u8").seek((jwplayer("playerm3u8").getPosition() + 10));
          }
        })
      },
      svgLabel: function (a) {
        let data = {
          "360p": `<svg class="jw-svg-icon jw-svg-icon-qswitch" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 24"><path d="M7 15v-1.5A1.5 1.5 0 0 0 5.5 12 1.5 1.5 0 0 0 7 10.5V9a2 2 0 0 0-2-2H1v2h4v2H3v2h2v2H1v2h4a2 2 0 0 0 2-2M10 7a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2h-2V9h4V7h-4m0 6h2v2h-2v-2zM17 7a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-2m0 2h2v6h-2V9zM28 7v10h2v-4h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-4m2 2h2v2h-2V9m-6-6h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H24a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2z"/></svg>`,
          "480p": `<svg class="jw-svg-icon jw-svg-icon-qswitch" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 24"><path d="M1 7v6h4v4h2V7H5v4H3V7H1zM10 13h2v2h-2m0-6h2v2h-2m0 6h2a2 2 0 0 0 2-2v-1.5a1.5 1.5 0 0 0-1.5-1.5 1.5 1.5 0 0 0 1.5-1.5V9a2 2 0 0 0-2-2h-2a2 2 0 0 0-2 2v1.5A1.5 1.5 0 0 0 9.5 12 1.5 1.5 0 0 0 8 13.5V15a2 2 0 0 0 2 2M17 7a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-2m0 2h2v6h-2V9zM28 7v10h2v-4h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-4m2 2h2v2h-2V9m-6-6h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H24a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2z"/></svg>`,
          "720p": `<svg class="jw-svg-icon jw-svg-icon-qswitch" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 24"><path d="M3 17l4-8V7H1v2h4l-4 8M8 7v2h4v2h-2a2 2 0 0 0-2 2v4h6v-2h-4v-2h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2H8zM17 7a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-2m0 2h2v6h-2V9zM28 7v10h2v-4h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-4m2 2h2v2h-2V9m-6-6h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H24a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2z"/></svg>`,
          "1080p": `<svg class="jw-svg-icon jw-svg-icon-qswitch" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 24"><path d="M2 7v2h2v8h2V7H2zM10 7a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-2m0 2h2v6h-2V9zM17 13h2v2h-2m0-6h2v2h-2m0 6h2a2 2 0 0 0 2-2v-1.5a1.5 1.5 0 0 0-1.5-1.5 1.5 1.5 0 0 0 1.5-1.5V9a2 2 0 0 0-2-2h-2a2 2 0 0 0-2 2v1.5a1.5 1.5 0 0 0 1.5 1.5 1.5 1.5 0 0 0-1.5 1.5V15a2 2 0 0 0 2 2M24 7a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-2m0 2h2v6h-2V9zM36 7v10h2v-4h2a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-4m2 2h2v2h-2V9m-6-6h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H32a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2z"/></svg>`,
        }
        return data[a];
      },
    };
    window.mobileAndTabletCheck = function() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };
  }
  